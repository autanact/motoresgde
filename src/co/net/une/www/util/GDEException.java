package co.net.une.www.util;

public class GDEException extends Exception {
	
	private String mensage;

	public GDEException(String mensage) {
		super();
		this.mensage = mensage;
	}

	public GDEException() {
		super();
		this.mensage = null;
	}
	
	public String getMensage() {
		return mensage;
	}

	public void setMensage(String mensage) {
		this.mensage = mensage;
	}

	
}

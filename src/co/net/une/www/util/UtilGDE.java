package co.net.une.www.util;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class UtilGDE {
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.UtilGDE", Locale.getDefault());
	private static final String formatoFechaInvalido = rb.getString("error.formatoFechaInvalido");

	public static String convertirFecha(String formato, String fecha) throws GDEException{
		String nuevaFecha = null;
		if (fecha!=null){
			SimpleDateFormat sdf = new SimpleDateFormat(formato);
			Calendar c = Calendar.getInstance();
			try {
				c.setTime(sdf.parse(fecha));
			} catch (ParseException e) {
				GDEException gdeException = new GDEException(MessageFormat.format(formatoFechaInvalido,formato,fecha));
				e.printStackTrace();
				throw gdeException;
			}
			nuevaFecha = sdf.format(c.getTime());
		}
		return nuevaFecha; 
		
	}
	
	public static Date convertirObjetoFecha(String formato, String fechaStr) throws GDEException {
        Date date = null;
        try {
			date = new SimpleDateFormat(formato).parse(fechaStr);
		} catch (ParseException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(formatoFechaInvalido,formato,fechaStr));
			throw gdeException;
		}	
		return date; 
	}
	
	
	public static boolean estaEnRango(String horaActual, String horaInicial, String horaFinal) throws GDEException{
		
		boolean enRango = true;
		Date horaAct = UtilGDE.convertirObjetoFecha("HHmm", horaActual);
		Date horaIni = UtilGDE.convertirObjetoFecha("HHmm", horaInicial);
		Date horaFin = UtilGDE.convertirObjetoFecha("HHmm", horaFinal);
		if (horaFin.before(horaIni)){
			Date medNoche = UtilGDE.convertirObjetoFecha("HHmm","2359");
			Date medNoche1 = UtilGDE.convertirObjetoFecha("HHmm","0000");
			enRango= (horaAct.after(horaIni) && horaAct.before(medNoche)) || (horaAct.after(medNoche1) && horaAct.before(horaFin)) ;
		}else{
			enRango = horaAct.after(horaIni) && horaAct.before(horaFin) ;
		}
		return enRango;
	}
	
	public static String colocarMascara(String valor) {
		// TODO Auto-generated method stub
		return valor==null? "":valor;
	}
}

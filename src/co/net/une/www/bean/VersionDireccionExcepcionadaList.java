package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: VersionDireccionExcepcionadaList.java
##	Contenido: Clase que implementa el objeto  de una lista ID de versi�n direcci�n excepcionada, relacionado con la tabla ds_cd_version_direxc_lt.
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Creaci�n de la clase
##**********************************************************************************************************************************
*/
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import co.net.une.www.util.GDEException;
/**
 * Clase que implementa el objeto  de una lista ID de versi�n direcci�n excepcionada, relacionado con la tabla ds_cd_version_direxc_lt.
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class VersionDireccionExcepcionadaList extends Servicios {
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.VersionDireccionExcepcionadaLoteList", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.servicio.mensaje");
	private static final String DATASET = rb.getString("VersionDireccionExcepcionadaLoteList.dataset"); 
	private static final String TABLE =  rb.getString("VersionDireccionExcepcionadaLoteList.tabla"); 
	
	
	private List<VersionDireccionExcepcionada> verDirExcLoteList = null;
	

	public VersionDireccionExcepcionadaList() {
		super();
		verDirExcLoteList = new ArrayList<VersionDireccionExcepcionada>();
	}

	public List<VersionDireccionExcepcionada> getVerDirExcLoteList() {
		return verDirExcLoteList;
	}

	public void setVerDirExcLoteList(List<VersionDireccionExcepcionada> verDirExcLoteList) {
		this.verDirExcLoteList = verDirExcLoteList;
	}
	
	public void incluirVerDirExcLote(VersionDireccionExcepcionada verDirExcLote){
		getVerDirExcLoteList().add(verDirExcLote);
	}
	
	public int totalVerDirExcLote(){
		return getVerDirExcLoteList()==null?0:getVerDirExcLoteList().size();
	}

	@Override
	protected String obtenerDateSet() {
		return DATASET;
	}

	@Override
	protected String obtenerTabla() {
		return TABLE;
	}

	@Override
	protected void obtenerObjeto(String registros) throws GDEException {
		if (registros != null && !"0".equals(registros)){
			String[] registro = registros.split("\\|");
			for (int i=0; i < registro.length;i++){
				registro[i] = registro[i].replaceAll("[\\[\\]]", "");
				//Coloco ## para identificar los campos que vienen vac�os
				while(registro[i].indexOf(";;")>=0){
					registro[i] = registro[i].replaceAll(";;", ";##;");
				}
				if (registro[i].endsWith(";")) registro[i] = registro[i]+"##";
				
				String[] campos = registro[i].split(";");
				VersionDireccionExcepcionada verDirExcLote = new VersionDireccionExcepcionada();
				verDirExcLote.setId(Integer.parseInt("##".equals(campos[0])?"0":campos[0]));
				incluirVerDirExcLote(verDirExcLote);
			}
		}
		
	}
	
	/**
	 * M�todo que obtiene un listado de versiones seg�n el id de la direcci�n excepcionada
	 * @param dirExcId
	 * 			Id de la direcci�n excepcionada
	 * @throws GDEException
	 */
	public void obtenerListaIdVerDirExcPorDirExcId(String dirExcId) throws GDEException {
		try {
			ejecutarServicioConsultaTabla("id", "direccion_excepcionada_id", dirExcId);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
	}

	@Override
	protected void obtenerObjeto(Object registros) throws GDEException {
		// TODO Auto-generated method stub
		
	}
	

}

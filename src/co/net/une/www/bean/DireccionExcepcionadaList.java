package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: DireccionExcepcionadaList.java
##	Contenido: Clase que implementa el objeto  de una lista ID de direcciones excepcionadas, relacionado con la tabla direccion_excepcionada
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;


import co.net.une.www.util.GDEException;

/**
 * Clase que implementa el objeto  de una lista ID de direcciones excepcionadas, relacionado con la tabla direccion_excepcionada
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class DireccionExcepcionadaList  extends Servicios {
	
	private List<DireccionExcepcionada> dirExcList = null;
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.DireccionExcepcionadaList", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.servicio.WSConsultarTablasGDE.noResponde");
	private static final String DATASET = rb.getString("direccionExcepcionadaList.dataset"); 
	private static final String TABLE =  rb.getString("direccionExcepcionadaList.tabla"); 

	public DireccionExcepcionadaList() {
		super();
		dirExcList = new ArrayList<DireccionExcepcionada>();
	}

	public List<DireccionExcepcionada> getDirExcList() {
		return dirExcList;
	}

	public void setDirExcList(List<DireccionExcepcionada> dirExcList) {
		this.dirExcList = dirExcList;
	}
	
	public int totalDirecciones(){
		return getDirExcList()==null?0:getDirExcList().size();
	}
	
	public void incluirDirExcepcionada(DireccionExcepcionada direccionExcepcionada){
		getDirExcList().add(direccionExcepcionada);
	}

	@Override
	protected String obtenerDateSet() {
		return DATASET;
	}

	@Override
	protected String obtenerTabla() {
		return TABLE;
	}

	@Override
	protected void obtenerObjeto(String registros) throws GDEException {
		if (registros != null && !"0".equals(registros)){
			String[] registro = registros.split("\\|");
			for (int i=0; i < registro.length;i++){
				registro[i] = registro[i].replaceAll("[\\[\\]]", "");
				//Coloco ## para identificar los campos que vienen vac�os
				while(registro[i].indexOf(";;")>=0){
					registro[i] = registro[i].replaceAll(";;", ";##;");
				}
				if (registro[i].endsWith(";")) registro[i] = registro[i]+"##";
				
				String[] campos = registro[i].split(";");
				DireccionExcepcionada dirExc = new DireccionExcepcionada();
				dirExc.setId(Integer.parseInt("##".equals(campos[0])?"0":campos[0]));
				incluirDirExcepcionada(dirExc);
			}
		}
		
	}

	/**
	 * M�todo que obtiene un listado con la condici�n de autorizada_gestion true o false
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void obtenerListaIdDirExcParaHistorico() throws GDEException {
		try {
			ejecutarServicioConsultaTabla("id", "autorizada_gestion", "{false;true}");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
		
	}
	
	/**
	 * M�todo que obtiene un listado seg�n los criterios seleccionados
	 * @param nombresCamposConsulta
	 * 			listado de nombres de los campos a consultar
	 * @param valoresCamposConsulta
	 * 			listado de los valores a consultar
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void obtenerListaIdDirExcSegunCriterio(String nombresCamposConsulta, String valoresCamposConsulta ) throws GDEException {
		try {
			ejecutarServicioConsultaTabla("id", nombresCamposConsulta, valoresCamposConsulta);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
		
	}

	@Override
	protected void obtenerObjeto(Object registros) throws GDEException {
	}

	public void obtenerListaDirExcIdParaLote() throws GDEException {
		try {
			ejecutarServicioConsultaTabla("id", "estado_gestion_type", "PENDIENTE");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
		
	}

}

package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: PeridiocidadDirecciones.java
##	Contenido: Clase que implementa el objeto Periodicidad Direcciones, relacionado con la tabla ds_cd_periodicidad_direcciones.
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Creaci�n de la clase
##**********************************************************************************************************************************
*/
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import co.net.une.www.util.GDEException;
import co.net.une.www.util.UtilGDE;
/**
 * Clase que implementa el objeto Periodicidad Direcciones, relacionado con la tabla ds_cd_periodicidad_direcciones.
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class PeridiocidadDirecciones extends Servicios{

	private int id;
	private String horaInicial;
	private String horaFinal;
	private String periodicidad;
	private String dias;
	private int tamanoLote;

	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.PeriocidadDirecciones", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.messageGSSException");
	private static final String DATASET = rb.getString("PeriocidadDirecciones.dataset"); 
	public static final String TABLE =  rb.getString("PeriocidadDirecciones.tabla");
	private static final String ALLFIELDS =  rb.getString("PeriocidadDirecciones.campos");
	
	
	
	public PeridiocidadDirecciones(int id, String horaInicial, String horaFinal, String periodicidad, String dias, int tamanoLote) {
		this.id = id;
		this.horaInicial = horaInicial;
		this.horaFinal = horaFinal;
		this.periodicidad = periodicidad;
		this.dias = dias;
		this.tamanoLote = tamanoLote;
	}
	
	
	public PeridiocidadDirecciones() {
		this.id = 0;
		this.horaInicial = null;
		this.horaFinal = null;
		this.periodicidad = null;
		this.dias = null;
		this.tamanoLote = 0;
	}


	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getHoraInicial() {
		return horaInicial;
	}
	
	public void setHoraInicial(String horaInicial) throws GDEException {
		this.horaInicial = UtilGDE.convertirFecha("HHmm", horaInicial);
	}
	 
	public String getHoraFinal() {
		return horaFinal;
	}
	
	public void setHoraFinal(String horaFinal) throws GDEException {
		this.horaFinal =  UtilGDE.convertirFecha("HHmm",horaFinal) ;
	}
	
	public String getPeriodicidad() {
		return periodicidad;
	}
	
	public void setPeriodicidad(String periodicidad) {
		this.periodicidad = periodicidad;
	}
	
	public String getDias() {
		return dias;
	}
	
	public void setDias(String dias) {
		this.dias = dias;
	}
	
	public int getTamanoLote() {
		return tamanoLote;
	}
	
	public void setTamanoLote(int tamanoLote) {
		this.tamanoLote = tamanoLote;
	}
	
	
	public void obtenerPeriodicidadDirecciones() throws GDEException{
		try {
			ejecutarServicioConsultaTabla(ALLFIELDS, null, null);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}
	
	/**
	 * Funci�n que indica si la ejecuci�n est� en rango para ser ejecutada. Depende del d�a y la hora
	 * @param dia
	 * 			d�a de ejecuci�n
	 * @param hora
	 * 			hora de ejecuci�n
	 * @return
	 * 		verdadero si la ejecuci�n est� en rango para ser ejecutada. Falso en cualquier otro caso
	 * @throws GDEException
	 */
	public boolean estaEnRango(int dia, String hora) throws GDEException{
		Date horaAct = UtilGDE.convertirObjetoFecha("HHmm", hora);
		Date horaIni = UtilGDE.convertirObjetoFecha("HHmm",getHoraInicial());
		Date horaFin = UtilGDE.convertirObjetoFecha("HHmm",getHoraFinal());
		boolean enRango = esDiaActivo(dias, dia);
		if (enRango){
			if (horaFin.before(horaIni)){
				Date medNoche = UtilGDE.convertirObjetoFecha("HHmm","2359");
				Date medNoche1 = UtilGDE.convertirObjetoFecha("HHmm","0000");
				enRango= (horaAct.after(horaIni) && horaAct.before(medNoche)) || (horaAct.after(medNoche1) && horaAct.before(horaFin)) ;
			}else{
				enRango = horaAct.after(horaIni) && horaAct.before(horaFin) ;
			}
		}
		return enRango;
	}
	
	/**
	 * Funci�n que valida si el d�a est� en la lista de d�as de ejecuci�n suministrada.
	 * @param dias
	 * 			Listado de d�as de ejecuci�n
	 * @param dia
	 * 			d�a de ejecuci�n a evaluar
	 * @return
	 * 		verdadero si el d�a est� en la lista de d�as de ejecuci�n suministrada. Falso en cualquier otro caso
	 */
	private boolean esDiaActivo(String dias, int dia){
		String diaAct = convertirDia(dia);
		String[] diaArr = dias.split(",");
		
		List<String> diaList = Arrays.asList(diaArr);
		return diaList.contains(diaAct);
	}
	
	/**
	 * Funci�n que retorna el nombre del d�a que representa el n�mero ingresado
	 * @param dia
	 * 			n�mero de d�as
	 * @return
	 * 		nombre del d�a que representa el n�mero ingresado
	 */
	private String convertirDia(int dia){
		String diaStr = null;
		switch (dia) {
		case Calendar.SUNDAY:
			diaStr = "DO";
			break;
		case Calendar.MONDAY:
			diaStr = "LU";
			break;
		case Calendar.TUESDAY:
			diaStr = "MA";
			break;
		case Calendar.WEDNESDAY:
			diaStr = "MI";
			break;
		case Calendar.THURSDAY:
			diaStr = "JU";
			break;
		case Calendar.FRIDAY:
			diaStr = "VI";
			break;
		case Calendar.SATURDAY:
			diaStr = "SA";
			break;
		default:
			break;
		}
		return diaStr;
	}
	

	@Override
	protected String obtenerDateSet() {
		return DATASET;
	}


	@Override
	protected String obtenerTabla() {
		return TABLE;
	}


	@Override
	protected void obtenerObjeto(String registros) throws GDEException {
		if (registros != null && !"0".equals(registros)){
			String[] registro = registros.split("\\|");
			for (int i=0; i < registro.length;i++){
				registro[i] = registro[i].replaceAll("[\\[\\]]", "");
				//Coloco ## para identificar los campos que vienen vac�os
				while(registro[i].indexOf(";;")>=0){
					registro[i] = registro[i].replaceAll(";;", ";##;");
				}
				if (registro[i].endsWith(";")) registro[i] = registro[i]+"##";
				String campos[] = registro[i].split(";");
				setId(Integer.parseInt(campos[0]));
				setHoraInicial(campos[1]);
				setHoraFinal(campos[2]);
				setPeriodicidad(campos[3]);
				setDias(campos[4]);
				setTamanoLote(Integer.parseInt(campos[5]));
			}
		}
		
	}


	@Override
	protected void obtenerObjeto(Object registros) throws GDEException {
		
	}
}

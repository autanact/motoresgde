package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: HistoricoDireccionExcepcionada.java
##	Contenido: Clase que implementa el objeto hist�rico direcci�n excepcionada, relacionado con la tabla ds_cd_hist_direxc_lt
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.util.Locale;

import java.util.ResourceBundle;

import co.net.une.www.util.GDEException;
import co.net.une.www.util.UtilGDE;

/**
 * Clase que implementa el objeto hist�rico direcci�n excepcionada, relacionado con la tabla ds_cd_hist_direxc_lt
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class HistoricoDireccionExcepcionada extends DireccionExcepcionada{
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.HistoricoDireccionExcepcionadaLote", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.messageGSSException");
	private static final String DATASET = rb.getString("HistoricoDireccionExcepcionadaLote.dataset"); 
	public static final String TABLE =  rb.getString("HistoricoDireccionExcepcionadaLote.tabla");
	
	private String nombreArchivo = null;
	

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public HistoricoDireccionExcepcionada() {
		super();
	}

	@Override
	public String obtenerDateSet() {
		return DATASET;
	}

	@Override
	public String obtenerTabla() {
		return TABLE;
	}

	@Override
	public void obtenerObjeto(String registros) throws GDEException {
		if (registros != null && !"0".equals(registros)){
			String[] registro = registros.split("\\|");
			for (int i=0; i < registro.length;i++){
				registro[i] = registro[i].replaceAll("[\\[\\]]", "");
				//Coloco ## para identificar los campos que vienen vac�os
				while(registros.indexOf(";;")>=0){
					registros = registros.replaceAll(";;", ";##;");
				}
				if (registros.endsWith(";")) registros = registros+"##";

			}
		}
	}

	/**
	 * M�todo que migra los datos de la direcci�n excepcionada al objeto HistoricoDireccionExcepcionada
	 * @param dirExc
	 * 			Objeto direcci�n excepcionda
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void migrarDireccionExcepcionada(DireccionExcepcionada dirExc) throws GDEException {
		setDepartamento(dirExc.getDepartamento());
		setCodigoSolicitud(dirExc.getCodigoSolicitud());
		setMunicipio(dirExc.getMunicipio());
		setRemanente(dirExc.getRemanente());
		setCodigoDireccion(dirExc.getCodigoDireccion());
		setComentario(dirExc.getComentario());
		setContacto(dirExc.getContacto());
		setCodigoSistema(dirExc.getCodigoSistema());
		setDireccionTextoLibre(dirExc.getDireccionTextoLibre());
		setDireccionNormalizada(dirExc.getDireccionNormalizada());
		setTelefonos(dirExc.getTelefonos());
		setUsuarioSolicitud(dirExc.getUsuarioSolicitud());
		setEstadoExcepcionType(dirExc.getEstadoExcepcionType());
		setPais(dirExc.getPais());
		setEstadoGeorType(dirExc.getEstadoGeorType());
		setFechaIngresoDireccion(UtilGDE.convertirFecha("dd/MM/yyyy", dirExc.getFechaIngresoDireccion())) ;
		setFechaEnvioProveedor(UtilGDE.convertirFecha("dd/MM/yyyy", dirExc.getFechaEnvioProveedor()));
		setFechaRespuestaProveedor(UtilGDE.convertirFecha("dd/MM/yyyy", dirExc.getFechaRespuestaProveedor()));
		setFechaEnvioSistema(UtilGDE.convertirFecha("dd/MM/yyyy",dirExc.getFechaEnvioSistema()));
		setEstadoGestionType(dirExc.getEstadoGestionType());
		setAutorizadaGestion(dirExc.getAutorizadaGestion());
		setComentarioProveedor(dirExc.getComentarioProveedor());
		
	}
	
	/**
	 * M�todo que inserta un hist�rico de direcci�n exepcionada en BD
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void insertarHistoricoDirExc() throws GDEException {
		try {
			String nombreCampos = "";
			String valoresCampos = "";
			if (getDepartamento()!=null){
				nombreCampos+="departamento|";
				valoresCampos+=getDepartamento()+"|";
			}
			if (getMunicipio()!=null){
				nombreCampos+="municipio|";
				valoresCampos+=getMunicipio()+"|";
			}
			if (getRemanente()!=null&&!getRemanente().equals(" ")){
				nombreCampos+="remanente|";
				valoresCampos+=getRemanente()+"|";
			}
			if (getCodigoDireccion()!=null){
				nombreCampos+="codigo_direccion|";
				valoresCampos+=getCodigoDireccion()+"|";
			}
			if (getComentario()!=null&&!getComentario().equals(" ")){
				nombreCampos+="comentario|";
				valoresCampos+=getComentario()+"|";
			}
			if (getContacto()!=null){
				nombreCampos+="contacto|";
				valoresCampos+=getContacto()+"|";
			}
			if (getCodigoSistema()!=null){
				nombreCampos+="codigo_sistema|";
				valoresCampos+=getCodigoSistema()+"|";
			}
			if (getDireccionTextoLibre()!=null){
				nombreCampos+="direccion_texto_libre|";
				valoresCampos+=getDireccionTextoLibre()+"|";
			}		
			if (getCodigoSolicitud()!=null){
				nombreCampos+="codigo_solicitud|";
				valoresCampos+=getCodigoSolicitud()+"|";
			}
			if (getDireccionNormalizada()!=null){
				nombreCampos+="direccion_normalizada|";
				valoresCampos+=getDireccionNormalizada()+"|";
			}
			if (getTelefonos()!=null){
				nombreCampos+="telefonos|";
				valoresCampos+=getTelefonos()+"|";
			}
			if (getUsuarioSolicitud()!=null){
				nombreCampos+="usuario_solicitud|";
				valoresCampos+=getUsuarioSolicitud()+"|";
			}
			if (getEstadoExcepcionType()!=null){
				nombreCampos+="estado_excepcion_type|";
				valoresCampos+=getEstadoExcepcionType()+"|";
			}
			if (getPais()!=null){
				nombreCampos+="pais|";
				valoresCampos+=getPais()+"|";
			}
			if (getEstadoGeorType()!=null){
				nombreCampos+="estado_geor_type|";
				valoresCampos+=getEstadoGeorType()+"|";
			}
			if (getFechaIngresoDireccion()!=null){
				nombreCampos+="fecha_ingreso_direccion|";
				valoresCampos+=getFechaIngresoDireccion()+"|";
			}	
			if (getFechaEnvioProveedor()!=null){
				nombreCampos+="fecha_envio_proveedor|";
				valoresCampos+=getFechaEnvioProveedor()+"|";
			}
			if (getFechaRespuestaProveedor()!=null){
				nombreCampos+="fecha_respuesta_proveedor|";
				valoresCampos+=getFechaRespuestaProveedor()+"|";
			}
			if (getFechaEnvioSistema()!=null){
				nombreCampos+="fecha_envio_sistema|";
				valoresCampos+=getFechaEnvioSistema()+"|";
			}
			if (getEstadoGestionType()!=null){
				nombreCampos+="estado_gestion_type|";
				valoresCampos+=getEstadoGestionType()+"|";
			}
			if (getAutorizadaGestion()!=null){
				nombreCampos+="autorizada_gestion|";
				valoresCampos+=getAutorizadaGestion()+"|";
			}
			if (getNombreArchivo()!=null){
				nombreCampos+="nombre_archivo|";
				valoresCampos+=getNombreArchivo()+"|";
			}
			if (getComentarioProveedor()!=null){
				nombreCampos+="comentario_proveedor|";
				valoresCampos+=getComentarioProveedor()+"|";
			}
			
			nombreCampos=nombreCampos.endsWith("|")?nombreCampos.substring(0, nombreCampos.length() - 1) :nombreCampos;
			valoresCampos=valoresCampos.endsWith("|")?valoresCampos.substring(0, valoresCampos.length() - 1) :valoresCampos;
			
			setId(Integer.parseInt(ejecutarServicioInsertarElementoTabla(nombreCampos, valoresCampos)));
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}

}

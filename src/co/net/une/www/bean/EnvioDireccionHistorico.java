package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: EnvioDireccionHistorico.java
##	Contenido: Clase que implementa condiciones para el env�o de direcciones excepcionadas al hist�rico
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import co.net.une.www.util.GDEException;

/**
 * Clase que implementa condiciones para el env�o de direcciones excepcionadas al hist�rico
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class EnvioDireccionHistorico extends Servicios {

	private int id=0;
	private int dias =0;
	private String fechaInicialHistorico = null;
	private String fechaProximoHistorico = null;
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.EnvioDireccionHistorico", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.servicio.mensaje");
	private static final String formatoFechaInvalido = rb.getString("error.formatoFechaInvalido");
	private static final String DATASET = rb.getString("EnvioDireccionHistorico.dataset"); 
	private static final String TABLE =  rb.getString("EnvioDireccionHistorico.tabla"); 
	
	public EnvioDireccionHistorico() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}

	public String getFechaInicialHistorico() {
		return fechaInicialHistorico;
	}

	public void setFechaInicialHistorico(String fechaInicialHistorico) {
		this.fechaInicialHistorico = fechaInicialHistorico;
	}
	
	public String getFechaProximoHistorico() {
		return fechaProximoHistorico;
	}

	private void setFechaProximoHistorico(String fechaProximoHistorico) {
		this.fechaProximoHistorico = fechaProximoHistorico;
	}
	
	@Override
	public String obtenerDateSet() {
		return DATASET;
	}

	@Override
	public String obtenerTabla() {
		// TODO Auto-generated method stub
		return TABLE;
	}

	@Override
	public void obtenerObjeto(String registros) throws GDEException {
		registros = registros.replaceAll("[\\[\\]]", "");
		String campos[] = registros.split(";");	
		setId(Integer.parseInt(campos[0]));
		
		setDias(Integer.parseInt(campos[1]));
		setFechaInicialHistorico(campos[2]);
		
		//obtengo la fecha del pr�ximo env�o historico
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(getFechaInicialHistorico()));
		} catch (ParseException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(formatoFechaInvalido,getFechaInicialHistorico()));
			e.printStackTrace();
			throw gdeException;
		}
		c.add(Calendar.DATE, getDias()); 
		setFechaProximoHistorico(sdf.format(c.getTime())); 
		
	}
	
	/**
	 * M�todo que obtiene los datos el env�o a hist�rico configurado en BD
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void obtenerEnvioDireccionHistorico() throws GDEException {
		
		try {
			ejecutarServicioConsultaTabla("id|dias|fecha_inicial_historico", null, null);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
		
	}

	/**
	 * Funci�n que retorna si el env�o al hist�rico est� en su d�a de ejecuci�n
	 * @return
	 * 		verdadero si el env�o al hist�rico est� en su d�a de ejecuci�n, falso en cualquier otro caso
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public boolean diaEjecucion() throws GDEException {
		
		//Obtengo la hora del sistema
        Calendar calNow = Calendar.getInstance();
        calNow.setTime(new Date());
        
        try {
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Calendar calNext = Calendar.getInstance();
			calNext.setTime(sdf.parse(getFechaInicialHistorico()));
			
			return (calNow.get(Calendar.DAY_OF_MONTH)== calNext.get(Calendar.DAY_OF_MONTH)) && (calNow.get(Calendar.MONTH)== calNext.get(Calendar.MONTH))
					&& (calNow.get(Calendar.YEAR)== calNext.get(Calendar.YEAR));
			
		} catch (ParseException e) {
			GDEException gdeException = new GDEException(messageGSSException+" "+MessageFormat.format(formatoFechaInvalido,getFechaInicialHistorico()));
			e.printStackTrace();
			throw gdeException;
		}
        
	}

	/**
	 * M�todo que actualiza en BD la pr�xima ejecuci�n del env�o al hist�rico. Se actualiza seg�n los d�as configurados
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void actualizarProximaEjecucion() throws GDEException {
		//obtengo la fecha del pr�ximo env�o historico
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(getFechaInicialHistorico()));
		} catch (ParseException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(formatoFechaInvalido,getFechaInicialHistorico()));
			e.printStackTrace();
			throw gdeException;
		}
		c.add(Calendar.DATE, getDias()); 
		setFechaInicialHistorico(sdf.format(c.getTime())); 
		
		try {
			ejecutarServicioActualizarElementoTabla("id", getId()+"", "fecha_inicial_historico", getFechaInicialHistorico());
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
	}

	@Override
	protected void obtenerObjeto(Object registros) throws GDEException {
		// TODO Auto-generated method stub
		
	}

}
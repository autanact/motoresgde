package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: VersionDireccionExcepcionada.java
##	Contenido: Clase que implementa el objeto versi�n direcci�n excepcionada, relacionado con la tabla ds_cd_version_direxc_lt.
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Creaci�n de la clase
##**********************************************************************************************************************************
*/
import java.util.Locale;
import java.util.ResourceBundle;

import co.net.une.www.util.GDEException;
/**
 * Clase que implementa el objeto versi�n direcci�n excepcionada, relacionado con la tabla ds_cd_version_direxc_lt.
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class VersionDireccionExcepcionada extends Servicios {
	
	private String fecha_ingreso_direccion = null;
	private int id = 0;
	private String fecha_envio_proveedor = null;
	private String fecha_modificacion = null;
	private String fecha_envio_sistema = null;
	private String estado_gestion_type = null;
	private String nombre_archivo = null;
	private String estado_excepcion = null;
	private String estado_georeferenciacion = null;
	private int direccion_excepcionada_id = 0;
	private String fechaRespuestaProveedor = null;
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.VersionDireccionExcepcionada", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.servicio.mensaje");
	private static final String DATASET = rb.getString("versionDireccionExcepcionada.dataset"); 
	private static final String TABLE =  rb.getString("versionDireccionExcepcionada.tabla"); 
	private static final String ALLFIELDS =  rb.getString("versionDireccionExcepcionada.campos");
	
	public VersionDireccionExcepcionada() {
		super();
	}

	public String getFechaIngresoDireccion() {
		return fecha_ingreso_direccion;
	}

	public void setFechaIngresoDireccion(String fecha_ingreso_direccion) {
		this.fecha_ingreso_direccion = fecha_ingreso_direccion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFechaEnvioProveedor() {
		return fecha_envio_proveedor;
	}

	public void setFechaEnvioProveedor(String fecha_envio_proveedor) {
		this.fecha_envio_proveedor = fecha_envio_proveedor;
	}

	public String getFechaModificacion() {
		return fecha_modificacion;
	}

	public void setFechaModificacion(String fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

	public String getFechaEnvioSistema() {
		return fecha_envio_sistema;
	}

	public void setFechaEnvioSistema(String fecha_envio_sistema) {
		this.fecha_envio_sistema = fecha_envio_sistema;
	}

	public String getEstadoGestionType() {
		return estado_gestion_type;
	}

	public void setEstadoGestionType(String estado_gestion_type) {
		this.estado_gestion_type = estado_gestion_type;
	}

	public String getNombreArchivo() {
		return nombre_archivo;
	}

	public void setNombreArchivo(String nombre_archivo) {
		this.nombre_archivo = nombre_archivo;
	}

	public String getEstadoExcepcion() {
		return estado_excepcion;
	}

	public void setEstadoExcepcion(String estado_excepcion) {
		this.estado_excepcion = estado_excepcion;
	}

	public String getEstadoGeoreferenciacion() {
		return estado_georeferenciacion;
	}

	public void setEstadoGeoreferenciacion(String estado_georeferenciacion) {
		this.estado_georeferenciacion = estado_georeferenciacion;
	}

	public int getDireccionExcepcionadaId() {
		return direccion_excepcionada_id;
	}

	public void setDireccionExcepcionadaId(int direccion_excepcionada_id) {
		this.direccion_excepcionada_id = direccion_excepcionada_id;
	}

	public String getFechaRespuestaProveedor() {
		return fechaRespuestaProveedor;
	}

	public void setFechaRespuestaProveedor(String fechaRespuestaProveedor) {
		this.fechaRespuestaProveedor = fechaRespuestaProveedor;
	}

	/**
	 * M�todo que inserta la versi�n de direcci�n excepcionda en la BD
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void insertarVerDirExec() throws GDEException {
		
		try {
			
			// Se inserta la versi�n en BD
			String campos = "fecha_ingreso_direccion|fecha_modificacion|estado_gestion_type|nombre_archivo|estado_excepcion|direccion_excepcionada_id";
			campos += (getFechaEnvioProveedor()!=null?"|fecha_envio_proveedor":"");
			String valores = getFechaIngresoDireccion()+"|"+getFechaModificacion()+"|"+getEstadoGestionType()+"|"+getNombreArchivo()+"|"+getEstadoExcepcion()+"|"+getDireccionExcepcionadaId();
			valores += (getFechaEnvioProveedor()!=null?"|"+getFechaEnvioProveedor():"");
			
			String id = ejecutarServicioInsertarElementoTabla(campos, valores);
			
			setId(Integer.parseInt(id));
			
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
		
		try {
			
			// Se relaciona con la direcci�n excepcionda
			ejecutarServicioRelacionarElementoTabla(DireccionExcepcionada.TABLE, getId()+"", getDireccionExcepcionadaId()+""); 
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}

	/**
	 * M�todo que obtiene una versi�n de direcci�n excepcionada
	 * @param verDirExcId
	 * 			Id de la versi�n de la direcci�n excepcionada
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void obtenerVerDirExcPorId(int verDirExcId) throws GDEException {
		try {
			ejecutarServicioConsultaTabla(ALLFIELDS, "id", verDirExcId+"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
	}

	@Override
	protected String obtenerDateSet() {
		return DATASET;
	}

	@Override
	protected String obtenerTabla() {
		return TABLE;
	}

	@Override
	protected void obtenerObjeto(String registros) throws GDEException {
		if (registros != null && !"0".equals(registros)){
			String[] registro = registros.split("\\|");
			for (int i=0; i < registro.length;i++){
				registro[i] = registro[i].replaceAll("[\\[\\]]", "");
				//Coloco ## para identificar los campos que vienen vac�os
				while(registro[i].indexOf(";;")>=0){
					registro[i] = registro[i].replaceAll(";;", ";##;");
				}
				if (registro[i].endsWith(";")) registro[i] = registro[i]+"##";
				
				String[] campos = registro[i].split(";");
				setId(Integer.parseInt("##".equals(campos[0])?"0":campos[0]));
				setFechaIngresoDireccion("##".equals(campos[1])?null:campos[1]);
				setFechaEnvioProveedor("##".equals(campos[2])?null:campos[2]);
				setFechaRespuestaProveedor("##".equals(campos[3])?null:campos[3]);
				setFechaModificacion("##".equals(campos[4])?null:campos[4]);
				setFechaEnvioSistema("##".equals(campos[5])?null:campos[5]);
				setEstadoGestionType("##".equals(campos[6])?null:campos[6]);
				setNombreArchivo("##".equals(campos[7])?null:campos[7]);
				setEstadoExcepcion("##".equals(campos[8])?null:campos[8]);
				setEstadoGeoreferenciacion("##".equals(campos[9])?null:campos[9]);
				setDireccionExcepcionadaId(Integer.parseInt("##".equals(campos[10])?"0":campos[10]));

			}
		}
		
	}

	/**
	 * Elimina una versi�n de la direcci�n excepcionada de la BD
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void eliminarVerDirExc() throws GDEException {

		try {
			ejecutarServicioEliminarElementoTabla("id", getId()+"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
	}

	@Override
	protected void obtenerObjeto(Object registros) throws GDEException {
		// TODO Auto-generated method stub
		
	}
	

}

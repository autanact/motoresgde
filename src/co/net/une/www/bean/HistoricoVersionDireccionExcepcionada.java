package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: HistoricoVersionDireccionExcepcionada.java
##	Contenido: Clase que implementa el objeto hist�rico versi�n direcci�n excepcionada, relacionado con la tabla ds_cd_histo_version_direxc_lt
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.util.Locale;
import java.util.ResourceBundle;

import co.net.une.www.util.GDEException;
/**
 * Clase que implementa el objeto hist�rico versi�n direcci�n excepcionada, relacionado con la tabla ds_cd_histo_version_direxc_lt
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class HistoricoVersionDireccionExcepcionada extends VersionDireccionExcepcionada {
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.HistoricoVersionDireccionExcepcionadaLote", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.messageGSSException");
	private static final String DATASET = rb.getString("HistoricoVersionDireccionExcepcionadaLote.dataset"); 
	public static final String TABLE =  rb.getString("HistoricoVersionDireccionExcepcionadaLote.tabla");
	private static final String ALLFIELDS =  rb.getString("HistoricoVersionDireccionExcepcionadaLote.campos");
	
	private int idHistDirExc = 0;
	
	public HistoricoVersionDireccionExcepcionada() {
		super();
	}
	
	public int getIdHistDirExc() {
		return idHistDirExc;
	}

	public void setIdHistDirExc(int idHistDirExc) {
		this.idHistDirExc = idHistDirExc;
	}
	
	/**
	 * Migra los datos del objeto VersionDireccionExcepcionada al objeto HistoricoVersionDireccionExcepcionada
	 * @param verDirExc
	 * 			Objeto VersionDireccionExcepcionada
	 */
	public void migrarVersionDireccionExcepcionada(VersionDireccionExcepcionada verDirExc) {
		setFechaIngresoDireccion(verDirExc.getFechaIngresoDireccion());
		setFechaEnvioProveedor(verDirExc.getFechaEnvioProveedor());
		setFechaRespuestaProveedor(verDirExc.getFechaRespuestaProveedor());
		setEstadoGestionType(verDirExc.getEstadoGestionType());
		setNombreArchivo(verDirExc.getNombreArchivo());
		setFechaEnvioSistema(verDirExc.getFechaEnvioSistema());
		setEstadoExcepcion(verDirExc.getEstadoExcepcion());
		setEstadoGeoreferenciacion(verDirExc.getEstadoGeoreferenciacion());
		
	}

	@Override
	protected String obtenerDateSet() {
		return DATASET;
	}

	@Override
	protected String obtenerTabla() {
		return TABLE;
	}

	@Override
	protected void obtenerObjeto(String registros) throws GDEException {
		if (registros != null && !"0".equals(registros)){
			String[] registro = registros.split("\\|");
			for (int i=0; i < registro.length;i++){
				registro[i] = registro[i].replaceAll("[\\[\\]]", "");
				//Coloco ## para identificar los campos que vienen vac�os
				while(registro[i].indexOf(";;")>=0){
					registro[i] = registro[i].replaceAll(";;", ";##;");
				}
				if (registro[i].endsWith(";")) registro[i] = registro[i]+"##";
				
				String[] campos = registro[i].split(";");
				setId(Integer.parseInt("##".equals(campos[0])?"0":campos[0]));
				setFechaIngresoDireccion("##".equals(campos[1])?null:campos[1]);
				setFechaEnvioProveedor("##".equals(campos[2])?null:campos[2]);
				setFechaRespuestaProveedor("##".equals(campos[3])?null:campos[3]);
				setFechaModificacion("##".equals(campos[4])?null:campos[4]);
				setFechaEnvioSistema("##".equals(campos[5])?null:campos[5]);
				setEstadoGestionType("##".equals(campos[6])?null:campos[6]);
				setNombreArchivo("##".equals(campos[7])?null:campos[7]);
				setEstadoExcepcion("##".equals(campos[8])?null:campos[8]);
				setEstadoGeoreferenciacion("##".equals(campos[9])?null:campos[9]);
				setDireccionExcepcionadaId(Integer.parseInt("##".equals(campos[10])?"0":campos[10]));

			}
		}
		
	}


	/**
	 * M�todo que ingresa un hist�rico de versi�n de direcci�n excepcionada a la BD
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void insertarHistVerDirExc() throws GDEException {
		try {
			String nombreCampos = "";
			String valoresCampos = "";
			if (getFechaIngresoDireccion()!=null){
				nombreCampos+="fecha_ingreso_direccion|";
				valoresCampos+=getFechaIngresoDireccion()+"|";
			}
			if (getFechaEnvioProveedor()!=null){
				nombreCampos+="fecha_envio_proveedor|";
				valoresCampos+=getFechaEnvioProveedor()+"|";
			}
			if (getFechaRespuestaProveedor()!=null){
				nombreCampos+="fecha_respuesta_proveedor|";
				valoresCampos+=getFechaRespuestaProveedor()+"|";
			}
			if (getEstadoGestionType()!=null){
				nombreCampos+="estado_gestion_type|";
				valoresCampos+=getEstadoGestionType()+"|";
			}
			if (getNombreArchivo()!=null){
				nombreCampos+="nombre_archivo|";
				valoresCampos+=getNombreArchivo()+"|";
			}
			if (getFechaEnvioSistema()!=null){
				nombreCampos+="fecha_envio_sistema|";
				valoresCampos+=getFechaEnvioSistema()+"|";
			}
			if (getEstadoExcepcion()!=null){
				nombreCampos+="estado_excepcion|";
				valoresCampos+=getEstadoExcepcion()+"|";
			}
			if (getEstadoGeoreferenciacion()!=null){
				nombreCampos+="estado_georeferenciacion|";
				valoresCampos+=getEstadoGeoreferenciacion()+"|";
			}
			if (getFechaModificacion()!=null){
				nombreCampos+="fecha_modificacion|";
				valoresCampos+=getFechaModificacion()+"|";
			}
			nombreCampos=nombreCampos.endsWith("|")?nombreCampos.substring(0, nombreCampos.length() - 1) :nombreCampos;
			valoresCampos=valoresCampos.endsWith("|")?valoresCampos.substring(0, valoresCampos.length() - 1) :valoresCampos;
			
			setId(Integer.parseInt(ejecutarServicioInsertarElementoTabla(nombreCampos, valoresCampos)));
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
		try {
			ejecutarServicioRelacionarElementoTabla(HistoricoDireccionExcepcionada.TABLE, getId()+"", getIdHistDirExc()+"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
	}
	
}

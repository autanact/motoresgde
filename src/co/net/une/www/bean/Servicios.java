package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: Servicios.java
##	Contenido: Clase abstracta que implementa la comunicaci�n a trav�s de servicios con la BD de SW y sistemas sat�lites 
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.axis2.AxisFault;

import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.svc.WSActualizarTablasGDEStub;
import co.net.une.www.svc.WSConsultarTablasGDEStub;
import co.net.une.www.svc.WSEliminarTablasGDEStub;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub;
import co.net.une.www.svc.WSInsertarTablasGDEStub;
import co.net.une.www.svc.WSRelacionarObjetosStub;
import co.net.une.www.svc.WSRespuestaDEStub;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQ;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQType;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRS;
import co.net.une.www.svc.WsActualizarEstadoDirExcStub;
import co.net.une.www.svc.WsCrearRegistroVersionDEStub;
import co.net.une.www.util.GDEException;
import co.net.une.www.util.UtilGDE;

/**
 * Clase abstracta que implementa la comunicaci�n a trav�s de servicios con la BD de SW y sistemas sat�lites
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public abstract class Servicios {
	
	//archivo de propiedades
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.Servicios", Locale.getDefault());
	// Mensajes de error de servicio WSConsultarTablasGDE
	private static final String messageServConsTabException = rb.getString("error.servicio.WSConsultarTablasGDE.noResponde");
	private static final String errorTecnicoServConsTabException = rb.getString("error.servicio.WSConsultarTablasGDE.errorTecnico");
	// Mensajes de error de servicio WSEliminarTablasGDE
	private static final String messageServElimTabException = rb.getString("error.servicio.WSEliminarTablasGDE.noResponde");
	private static final String errorTecnicoServElimTabException = rb.getString("error.servicio.WSEliminarTablasGDE.errorTecnico");	
	// Mensajes de error de servicio WSInsertarTablasGDE
	private static final String messageServInsTabException = rb.getString("error.servicio.WSInsertarTablasGDE.noResponde");
	private static final String errorTecnicoServInsTabException = rb.getString("error.servicio.WSInsertarTablasGDE.errorTecnico");	
	// Mensajes de error de servicio WSRelacionarObjetos
	private static final String messageServRelTabException = rb.getString("error.servicio.WSRelacionarObjetos.noResponde");
	private static final String errorTecnicoServRelTabException = rb.getString("error.servicio.WSRelacionarObjetos.errorTecnico");
	// Mensajes de error de servicio WSActualizarTablas
	private static final String messageServActTabException = rb.getString("error.servicio.WSActualizarTablasGDEStub.noResponde");
	private static final String errorTecnicoServActTabException = rb.getString("error.servicio.WSActualizarTablasGDEStub.errorTecnico");
	// Mensajes de error de servicio WSGeorreferenciarCR
	private static final String messageServGeoException = rb.getString("error.servicio.WSGeorreferenciarCR.noResponde");
	private static final String errorTecnicoServGeoException = rb.getString("error.servicio.WSGeorreferenciarCR.errorTecnico");
	// Mensajes de error de servicio WsActualizarEstadoDirExc
	private static final String messageServActDirNotifException = rb.getString("error.servicio.WsActualizarEstadoDirExcStub.noResponde");
	private static final String errorTecnicoServActDirNotifException = rb.getString("error.servicio.WsActualizarEstadoDirExcStub.errorTecnico");
	// Mensajes de error de servicio WsCrearRegistroVersionDE
	private static final String messageServCrearVersionDirException = rb.getString("error.servicio.WsCrearRegistroVersionDE.noResponde");
	private static final String errorTecnicoServCrearVersionDirException = rb.getString("error.servicio.WsCrearRegistroVersionDE.errorTecnico");
	// Tiempo en minutos para espera de respuesta antes de dar TimeOut el servicio
	private static final String minTimeOut = ServiciosUtil.getTimeOutServicios();
	
	/**
	 * Funci�n que retorna el nombre del dataset relacionado
	 * @return
	 * 		nombre del dataset relacionado
	 */
	abstract protected String obtenerDateSet();
	
	/**
	 * Funci�n que retorna el nombre de la tabla relacionada
	 * @return
	 * 		nombre de la tabla relacionada
	 */
	abstract protected String obtenerTabla();
	
	/**
	 * M�todo que crea un objeto a partir de un string con los valores de los campos
	 * @param registros
	 * 			registro con los datos a ingresar 
	 * @throws GDEException
	 */
	abstract protected void obtenerObjeto(String registros) throws GDEException ;
	
	/**
	 * M�todo que crea un objeto a partir de un objeto con los valores de los campos
	 * @param registros
	 * 			registro con los datos a ingresar 
	 * @throws GDEException
	 */
	abstract protected void obtenerObjeto(Object registros) throws GDEException ;
	
	/**
	 * Funci�n que retorna la cantidad de miliseg�ndos que espera el servicio antes de dar TimeOut
	 * @return
	 * 		cantidad de miliseg�ndos que espera el servicio antes de dar TimeOut
	 */
	private long obtenerServicioTimeOut(){
		return Integer.parseInt(minTimeOut) * 60 * 1000; // Two minutes
	}
	
	/**
	 * M�todo que se comunica y ejecuta el servicio WSConsultarTablasGDE.
	 * @param nombreCamposResultado
	 * 			listado de nombres de los campos a mostrar
	 * @param nombresCamposConsulta
	 * 			listado de nombres de los campos a filtrar
	 * @param valoresCamposConsulta
	 * 			listado de los valores a filtrar
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	protected void ejecutarServicioConsultaTabla(String nombreCamposResultado, String nombresCamposConsulta, String valoresCamposConsulta ) throws GDEException{
		try {
			WSConsultarTablasGDEStub proxy = new WSConsultarTablasGDEStub();
			WSConsultarTablasGDEStub.WSConsultarTablasGDERQ request = new WSConsultarTablasGDEStub.WSConsultarTablasGDERQ();
			WSConsultarTablasGDEStub.WSConsultarTablasGDERS response = null;
			
			request.setWSConsultarTablasGDERQ(new WSConsultarTablasGDEStub.WSConsultarTablasGDERQType());
			request.getWSConsultarTablasGDERQ().setNombreDataset(obtenerDateSet());
			request.getWSConsultarTablasGDERQ().setNombreTabla(obtenerTabla());
			//obtengo los campos a mostrar en el resultado
			if (nombreCamposResultado!=null)
				request.getWSConsultarTablasGDERQ().setNombreCamposResultado(nombreCamposResultado);
			//obtengo los campos de consulta
			if (nombresCamposConsulta!=null)
				request.getWSConsultarTablasGDERQ().setNombresCamposConsulta(nombresCamposConsulta);
			//obtengo los valores de los campos de consulta
			if (valoresCamposConsulta!=null)
				request.getWSConsultarTablasGDERQ().setValoresCamposConsulta(valoresCamposConsulta);

			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(obtenerServicioTimeOut());

			response = proxy.consultarTablasGDE(request);
			
			if (response !=null){
				
				if ("OK".equals(response.getWSConsultarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta())){
					String registros = response.getWSConsultarTablasGDERS().getRegistros();
					if (registros != null && !"0".equals(registros)){
						obtenerObjeto(registros);
					}
				}else{
					String errorTecnico = response.getWSConsultarTablasGDERS().getGisRespuestaProceso().getDescripcionError()+" ("+response.getWSConsultarTablasGDERS().getGisRespuestaProceso().getCodigoError()+")";
					GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServConsTabException,errorTecnico));
					throw gdeException;
				}
			}

		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServConsTabException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServConsTabException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}	
		
	}
	
	/**
	 * Funci�n que se comunica y ejecuta el servicio WSInsertarTablasGDE 
	 * @param nombresCampos
	 * 			listado de nombres de los campos a ingresar
	 * @param valoresCampos
	 * 			listado de los valores a ingresar
	 * @return
	 * 		ID del elemento insertado o mensaje de error
	 * @throws GDEException
	 * 				En caso de una falla en el servicio se dispara una excepci�n
	 */
	protected String ejecutarServicioInsertarElementoTabla(String nombresCampos, String valoresCampos) throws GDEException {
		try {
			WSInsertarTablasGDEStub proxy = new WSInsertarTablasGDEStub();
			WSInsertarTablasGDEStub.WSInsertarTablasGDERQ request = new WSInsertarTablasGDEStub.WSInsertarTablasGDERQ();
			WSInsertarTablasGDEStub.WSInsertarTablasGDERS response = null;
			
			request.setWSInsertarTablasGDERQ(new WSInsertarTablasGDEStub.WSInsertarTablasGDERQType());
			request.getWSInsertarTablasGDERQ().setNombreDataset(obtenerDateSet());
			request.getWSInsertarTablasGDERQ().setNombreTabla(obtenerTabla());
			request.getWSInsertarTablasGDERQ().setNombresCampos(nombresCampos); 
			request.getWSInsertarTablasGDERQ().setValoresCampos(valoresCampos);

			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(obtenerServicioTimeOut());

			response = proxy.insertarTablasGDE(request);
			
			
			if (!"OK".equals(response.getWSInsertarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta())){
				String errorTecnico = response.getWSInsertarTablasGDERS().getGisRespuestaProceso().getDescripcionError()+" ("+response.getWSInsertarTablasGDERS().getGisRespuestaProceso().getCodigoError()+")";
				GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServInsTabException,errorTecnico));
				throw gdeException;
			}
			
			return response.getWSInsertarTablasGDERS().getGisRespuestaProceso().getDescripcionError();
		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServInsTabException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServInsTabException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}

	}
	
	/**
	 * M�todo que se comunica y ejecuta el servicio WSRelacionarObjetos.
	 * @param tablaRelacionada
	 * 			nombre de tabla relacionada
	 * @param codigoBase
	 * 			valor del c�digo base a relacionar
	 * @param codigoRelacionado
	 * 			valor del c�digo relacionado
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	protected void ejecutarServicioRelacionarElementoTabla (String tablaRelacionada,String codigoBase,String codigoRelacionado) throws GDEException {
		try {
			WSRelacionarObjetosStub proxy = new WSRelacionarObjetosStub();
			WSRelacionarObjetosStub.WSRelacionarObjetosRQ request = new WSRelacionarObjetosStub.WSRelacionarObjetosRQ();
			WSRelacionarObjetosStub.WSRelacionarObjetosRS response = null;
			
			request.setWSRelacionarObjetosRQ(new WSRelacionarObjetosStub.WSRelacionarObjetosRQType());
			request.getWSRelacionarObjetosRQ().setDataset(obtenerDateSet());
			request.getWSRelacionarObjetosRQ().setTablaBase(obtenerTabla());
			request.getWSRelacionarObjetosRQ().setTablaRelacionada(tablaRelacionada);
			request.getWSRelacionarObjetosRQ().setCodigoBase(codigoBase);
			request.getWSRelacionarObjetosRQ().setCodigoRelacionado(codigoRelacionado);

			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(obtenerServicioTimeOut());

			response = proxy.relacionarObjetos(request);
			
			if (!"OK".equals(response.getWSRelacionarObjetosRS().getGisRespuestaProceso().getCodigoRespuesta())){
				String errorTecnico = response.getWSRelacionarObjetosRS().getGisRespuestaProceso().getDescripcionError()+" ("+response.getWSRelacionarObjetosRS().getGisRespuestaProceso().getCodigoError()+")";
				GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServRelTabException,errorTecnico));
				throw gdeException;
			}
			
		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServRelTabException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServRelTabException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}
	}
	
	/**
	 * M�todo que se comunica y ejecuta el servicio WSEliminarTablasGDE.
	 * @param nombreCampoClave
	 * 			listado de nombres de los campos a filtar
	 * @param valoreCampoClave
	 * 			listado de nombres de los campos a filtrar
	 * @return
	 * 		Cantidad de elementos eliminados o mensaje de error
	 * @throws GDEException
	 * 				En caso de una falla en el servicio se dispara una excepci�n
	 */
	protected String ejecutarServicioEliminarElementoTabla(String nombreCampoClave, String valoreCampoClave) throws GDEException {
		try {
			WSEliminarTablasGDEStub proxy = new WSEliminarTablasGDEStub();
			WSEliminarTablasGDEStub.WSEliminarTablasGDERQ request = new WSEliminarTablasGDEStub.WSEliminarTablasGDERQ();
			WSEliminarTablasGDEStub.WSEliminarTablasGDERS response = null;
			
			request.setWSEliminarTablasGDERQ(new WSEliminarTablasGDEStub.WSEliminarTablasGDERQType());
			request.getWSEliminarTablasGDERQ().setNombreDataset(obtenerDateSet());
			request.getWSEliminarTablasGDERQ().setNombreTabla(obtenerTabla());
			request.getWSEliminarTablasGDERQ().setNombreCampoClave(nombreCampoClave);
			request.getWSEliminarTablasGDERQ().setValorCampoClave(valoreCampoClave);

			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(obtenerServicioTimeOut());

			response = proxy.eliminarTablasGDE(request);
			
			if (!"OK".equals(response.getWSEliminarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta())){
				String errorTecnico = response.getWSEliminarTablasGDERS().getGisRespuestaProceso().getDescripcionError()+" ("+response.getWSEliminarTablasGDERS().getGisRespuestaProceso().getCodigoError()+")";
				GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServElimTabException,errorTecnico));
				throw gdeException;
			}
			
			return response.getWSEliminarTablasGDERS().getGisRespuestaProceso().getDescripcionError();
		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServElimTabException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServElimTabException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}

	}
	
	/**
	 * M�todo que se comunica y ejecuta el servicio WSActualizarTablasGDE.
	 * @param nombreCampoClave
	 * 			listado de nombres de los campos a filtrar
	 * @param valorCampoClave
	 * 			listado de los valores a filtrar
	 * @param nombresCamposAct
	 * 			listado de nombres de los campos a actualizar
	 * @param valoresCamposAct
	 * 			listado de los valores a actualizar
	 * @throws GDEException
	 * 				En caso de una falla en el servicio se dispara una excepci�n
	 */
	protected void ejecutarServicioActualizarElementoTabla(String nombreCampoClave, String valorCampoClave, String nombresCamposAct,String valoresCamposAct) throws GDEException {
		try {
			WSActualizarTablasGDEStub proxy = new WSActualizarTablasGDEStub();
			WSActualizarTablasGDEStub.WSActualizarTablasGDERQ request = new WSActualizarTablasGDEStub.WSActualizarTablasGDERQ();
			WSActualizarTablasGDEStub.WSActualizarTablasGDERS response = null;
			
			request.setWSActualizarTablasGDERQ(new WSActualizarTablasGDEStub.WSActualizarTablasGDERQType());
			request.getWSActualizarTablasGDERQ().setNombreDataset(obtenerDateSet());
			request.getWSActualizarTablasGDERQ().setNombreTabla(obtenerTabla());
			request.getWSActualizarTablasGDERQ().setNombreCampoClave(nombreCampoClave); 
			request.getWSActualizarTablasGDERQ().setValorCampoClave(valorCampoClave);
			request.getWSActualizarTablasGDERQ().setNombresCamposAct(nombresCamposAct);
			request.getWSActualizarTablasGDERQ().setValoresCamposAct(valoresCamposAct);

			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(obtenerServicioTimeOut());
			
			response = proxy.actualizarTablasGDE(request);

			if (!"OK".equals(response.getWSActualizarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta())){
				String errorTecnico = response.getWSActualizarTablasGDERS().getGisRespuestaProceso().getDescripcionError()+" ("+response.getWSActualizarTablasGDERS().getGisRespuestaProceso().getCodigoError()+")";
				GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServActTabException,errorTecnico));
				throw gdeException;
			}
			
		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServActTabException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServActTabException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}
	}
	
	/**
	 * M�todo que se comunica y ejecuta el servicio WSGeorreferenciarCRService.
	 * @param pais
	 * 			c�digo del pa�s
	 * @param departamento
	 * 			c�digo del departamento
	 * @param municipio
	 * 			c�digo del municipio
	 * @param direccionNormalizada
	 * 			Direcci�n Normalizada
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	protected void ejecutarServicioObtenerDirGeoreferenciada(String pais, String departamento, String municipio, String direccionNormalizada ) throws GDEException{
		
		try {
			WSGeorreferenciarCRServiceStub geoProxy = new WSGeorreferenciarCRServiceStub();
			WSGeorreferenciarCRRQ geoRequest = new WSGeorreferenciarCRRQ();
			WSGeorreferenciarCRRQType geoRequestType = new WSGeorreferenciarCRRQType();
			WSGeorreferenciarCRRS response = new WSGeorreferenciarCRRS();
			
			geoRequestType.setCodigoDepartamento(departamento);
			geoRequestType.setCodigoMunicipio(municipio);
			geoRequestType.setCodigoPais(pais);
			geoRequestType.setDireccionNatural(direccionNormalizada);
			geoRequestType.setLatitud("0");
			geoRequestType.setLongitud("0");
			geoRequestType.setUNE_Cobertura_Especial(0);
			
			geoRequest.setWSGeorreferenciarCRRQ(geoRequestType);

			geoProxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			geoProxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(obtenerServicioTimeOut());
			
			response = geoProxy.georeferenciarCR(geoRequest);
			if ("OK".equals(response.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getCodigoRespuesta()) && "".equals(response.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getDescripcionError())){
				
				WSGeorreferenciarCRServiceStub.GisCommonInfoDirType infoDirType = response.getWSGeorreferenciarCRRS().getGisCommonInfoDir();
				obtenerObjeto(infoDirType);
				
			}else{
				GDEException gdeException = new GDEException(response.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getDescripcionError());
				throw gdeException;
			}
			
		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServGeoException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServGeoException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}     
		
	}
	
	/**
	 * M�todo que se comunica y ejecuta el servicio WsActualizarEstadoDirExc.
	 * @param codigoDireccion
	 * 			C�digo de direcci�n
	 * @param estadoNotificacion
	 * 			Estado de notificaci�n
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	protected void ejecutarServicioActualizarEstadoDirExc(String codigoDireccion, String estadoNotificacion) throws GDEException {
		try {
			WsActualizarEstadoDirExcStub proxy = new WsActualizarEstadoDirExcStub();
			WsActualizarEstadoDirExcStub.WsActualizarEstadoDirExcRQ request = new WsActualizarEstadoDirExcStub.WsActualizarEstadoDirExcRQ();
			WsActualizarEstadoDirExcStub.WsActualizarEstadoDirExcRS response = null;
			
			request.setWsActualizarEstadoDirExcRQ(new WsActualizarEstadoDirExcStub.WsActualizarEstadoDirExcRQType());
			request.getWsActualizarEstadoDirExcRQ().setCodigoDireccion(codigoDireccion);
			request.getWsActualizarEstadoDirExcRQ().setEstadoNotificacion(estadoNotificacion);

			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(obtenerServicioTimeOut());
			
			response = proxy.actualizarEstadoDirExc(request);

			if (!"OK".equals(response.getWsActualizarEstadoDirExcRS().getGisRespuestaProceso().getCodigoRespuesta())){
				String errorTecnico = response.getWsActualizarEstadoDirExcRS().getGisRespuestaProceso().getDescripcionError()+" ("+response.getWsActualizarEstadoDirExcRS().getGisRespuestaProceso().getCodigoError()+")";
				GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServActDirNotifException,errorTecnico));
				throw gdeException;
			}
			
		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServActDirNotifException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServActDirNotifException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}
	}
	
	/**
	 * M�todo que se comunica y ejecuta el servicio WsCrearRegistroVersionDE
	 * @param codigoDireccion
	 * 			C�digo de direcci�n
	 * @param nombreArchivo
	 * 			Nombre del archivo de lote. Puede ser null
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	protected void ejecutarServicioCrearVersionDirExc(String codigoDireccion,String nombreArchivo) throws GDEException{
		try {
			WsCrearRegistroVersionDEStub proxy = new WsCrearRegistroVersionDEStub();
			WsCrearRegistroVersionDEStub.WsCrearRegistroVersionDERQ request = new WsCrearRegistroVersionDEStub.WsCrearRegistroVersionDERQ();
			WsCrearRegistroVersionDEStub.WsCrearRegistroVersionDERS response = null;
			
			request.setWsCrearRegistroVersionDERQ(new WsCrearRegistroVersionDEStub.WsCrearRegistroVersionDERQType());
			WsCrearRegistroVersionDEStub.BoundedString100 codDir = new WsCrearRegistroVersionDEStub.BoundedString100();
			WsCrearRegistroVersionDEStub.BoundedString100 arch = new WsCrearRegistroVersionDEStub.BoundedString100();
			codDir.setBoundedString100(codigoDireccion);
			request.getWsCrearRegistroVersionDERQ().setCodigoDireccion(codDir);
			
			// Valido que el nombre de archivo venga en la llamada
			if (nombreArchivo!= null && !"".equals(nombreArchivo)){
				arch.setBoundedString100(nombreArchivo);
				request.getWsCrearRegistroVersionDERQ().setNombreArchivo(arch);
			}

			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(obtenerServicioTimeOut());
			
			response = proxy.crearRegistroVersionDE(request);

			if (!"OK".equals(response.getWsCrearRegistroVersionDERS().getGisRespuestaProceso().getCodigoRespuesta())){
				String errorTecnico = response.getWsCrearRegistroVersionDERS().getGisRespuestaProceso().getDescripcionError()+" ("+response.getWsCrearRegistroVersionDERS().getGisRespuestaProceso().getCodigoError()+")";
				GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServCrearVersionDirException,errorTecnico));
				throw gdeException;
			}
			
		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServCrearVersionDirException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServCrearVersionDirException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}
		
	}
	
	/**
	 * M�todo que se comunica y ejecuta el servicio WSRespuestaDE.
	 * @param estrato
	 * 			Estrato
	 * @param codigoSolicitud
	 * 			C�digo de solicitud
	 * @param codigoSistema
	 * 			C�digo de Sistema
	 * @param estadoExcepcion
	 * 			Estado de Excepci�n
	 * @param codigoDireccion
	 * 			C�digo de direcci�n
	 * @param direccionNormalizada
	 * 			direccionNormalizada
	 * @param rural
	 * 			rural
	 * @param estadoGeo
	 * 			Estado de georreferencia
	 * @param pais
	 * 			C�digo de pa�s
	 * @param municipio
	 * 			C�digo de municipio
	 * @param departamento
	 * 			C�digo de departamento
	 * @return
	 * 		Respuesta del servicio de RespuestaDE (OK|KO)
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	protected String ejecutarServicioRespuestaDE(String estrato, String codigoSolicitud, String codigoSistema, String estadoExcepcion,
			String codigoDireccion, String direccionNormalizada, String rural,String estadoGeo, String pais, String municipio,
			String departamento, String latitud, String longitud) throws GDEException{
		
		try {
			
			// creamos el soporte y la peticion WS
			WSRespuestaDEStub proxy = new WSRespuestaDEStub();
			WSRespuestaDEStub.WSRespuestaDERQ  consultarRequest = new WSRespuestaDEStub.WSRespuestaDERQ();
			WSRespuestaDEStub.WSRespuestaDERQType  consultarRequestType = new WSRespuestaDEStub.WSRespuestaDERQType();
			WSRespuestaDEStub.WSRespuestaDERS consultarResponse = new WSRespuestaDEStub.WSRespuestaDERS();	       
					
			// establecemos el parametro de la invocacion 
			WSRespuestaDEStub.BoundedString15 csol = new WSRespuestaDEStub.BoundedString15();
			WSRespuestaDEStub.BoundedString30 csis = new WSRespuestaDEStub.BoundedString30();
			WSRespuestaDEStub.BoundedString30 eexc = new WSRespuestaDEStub.BoundedString30();
			WSRespuestaDEStub.BoundedString100 cdir = new WSRespuestaDEStub.BoundedString100();
			WSRespuestaDEStub.BoundedString250 dnor = new WSRespuestaDEStub.BoundedString250();
			BigInteger estra = new BigInteger("0");
			try {
				estra = new BigInteger(estrato);
			} catch (Exception e) {}
						 
			WSRespuestaDEStub.BoundedString2 ruralBs2 = new WSRespuestaDEStub.BoundedString2();
			WSRespuestaDEStub.BoundedString1 egeo = new WSRespuestaDEStub.BoundedString1();
			WSRespuestaDEStub.BoundedString2 paisBs2 = new WSRespuestaDEStub.BoundedString2();
			WSRespuestaDEStub.BoundedString8 munic = new WSRespuestaDEStub.BoundedString8();
			WSRespuestaDEStub.BoundedString2 depar = new WSRespuestaDEStub.BoundedString2();
									
						
			csol.setBoundedString15(codigoSolicitud);
			csis.setBoundedString30(codigoSistema);
			eexc.setBoundedString30(estadoExcepcion);
			cdir.setBoundedString100(codigoDireccion);
			dnor.setBoundedString250(direccionNormalizada);
			ruralBs2.setBoundedString2(rural);
			egeo.setBoundedString1(estadoGeo);
			paisBs2.setBoundedString2(pais);
			munic.setBoundedString8(municipio);
			depar.setBoundedString2(departamento);
			consultarRequestType.setCodigoSolicitud(csol);
			consultarRequestType.setCodigoSistema(csis);
			consultarRequestType.setEstadoExcepcion(eexc);
			consultarRequestType.setCodigoDireccion(cdir);
			consultarRequestType.setDireccionNormalizada(dnor);
			consultarRequestType.setEstrato(estra);
			consultarRequestType.setRural(ruralBs2);
			consultarRequestType.setEstadoGeoreferenciacion(egeo);
			consultarRequestType.setCodigoPais(paisBs2);
			consultarRequestType.setCodigoMunicipio(munic);
			consultarRequestType.setCodigoDepartamento(depar);
			consultarRequestType.setLatitud(latitud);
			consultarRequestType.setLongitud(longitud);
						
			// invocamos al web service
			consultarRequest.setWSRespuestaDERQ(consultarRequestType);
			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(obtenerServicioTimeOut());
			consultarResponse = proxy.respuestaDE(consultarRequest);
			
			if (!"".equals(consultarResponse.getWSRespuestaDERS().getGisRespuestaProceso().getDescripcionError())){
				String errorTecnico = consultarResponse.getWSRespuestaDERS().getGisRespuestaProceso().getDescripcionError();
				GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServCrearVersionDirException,errorTecnico));
				throw gdeException;
			}
			
			return consultarResponse.getWSRespuestaDERS().getGisRespuestaProceso().getCodigoRespuesta();
				
			
		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServCrearVersionDirException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServCrearVersionDirException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}
	}
	
	
	/**
	 * Funci�n que quita las mascara y retorna el valor
	 * @param valor
	 * 			valor a quitar la mascara
	 * @return
	 */
	protected String quitarMascaraTexto(String valor){
		return "##".equals(valor)?null:valor;
	}
	
	/**
	 * Funci�n que quita las mascara y retorna el valor
	 * @param valor
	 * 			valor a quitar la mascara
	 * @return
	 */
	protected int quitarMascaraEntero(String valor) {
		return "##".equals(valor) ? 0 : Integer.parseInt(valor);
	}

}

package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: HistoricoDireccionExcepcionadaList.java
##	Contenido: Clase que implementa el objeto  de una lista ID de hist�rico de direcciones excepcionadas, relacionado con la tabla ds_cd_hist_direxc_lt
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Clase que implementa el objeto  de una lista ID de hist�rico de direcciones excepcionadas, relacionado con la tabla ds_cd_hist_direxc_lt
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class HistoricoDireccionExcepcionadaList {
	
	private List<HistoricoDireccionExcepcionada> listDirExec = null;
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.DireccionExcepcionadaList", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.servicio.WSConsultarTablasGDE.noResponde");
	private static final String errorTecnicoGSSException = rb.getString("error.servicio.WSConsultarTablasGDE.errorTecnico");
	private static final String DATASET = rb.getString("direccionExcepcionadaList.dataset"); 
	private static final String TABLE =  rb.getString("direccionExcepcionadaList.tabla"); 

	public HistoricoDireccionExcepcionadaList() {
		super();
		setLisDirExec(new ArrayList<HistoricoDireccionExcepcionada>());
	}

	public List<HistoricoDireccionExcepcionada> getLisDirExec() {
		return listDirExec;
	}

	public void setLisDirExec(List<HistoricoDireccionExcepcionada> lisDirExec) {
		this.listDirExec = lisDirExec;
	}

	public void obtenerHistDirExcParaAlmacenar(int dias) {
		
		
	}
	
	

}

package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: DireccionExcepcionada.java
##	Contenido: Clase que implementa el objeto direcci�n excepcionada, relacionado con la tabla direccion_excepcionada
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.axis2.AxisFault;

import co.net.une.www.svc.WSRespuestaDEStub;
import co.net.une.www.util.GDEException;
import co.net.une.www.util.UtilGDE;
/**
 * Clase que implementa el objeto direcci�n excepcionada, relacionado con la tabla direccion_excepcionada
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class DireccionExcepcionada extends Direccion {
	
	private String detalleDireccion = null;
	private String fechaIngresoDireccion = null;
	private String fechaEnvioProveedor = null;
	private String estadoExcepcionType = null;
	private String fechaRespuestaProveedor = null;
	private String fechaEnvioSistema = null;
	private String estadoGestionType = null;
	private String autorizadaGestion = null;
	private String comentarioProveedor = null;
	private String codigoSolicitud = null;
	private String usuarioSolicitud = null;
	private String codigoSistema = null;
	private String direccionTextoLibre = null;
	private String contacto = null;
	private String telefonos = null;
	private String comentario = null;
	private int dirEnLotes = 0;
	private String direccionEstandarNLectura = null;
	private String estadoPagina = null;
	private String instalacion = null;
	private String loteId = null;
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.DireccionExcepcionada", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.messageGSSException");
	private static final String messageServResDEException = rb.getString("error.servicio.WSRespuestaDE.noResponde");
	private static final String errorTecnicoServResDEException = rb.getString("error.servicio.WSRespuestaDE.errorTecnico");
	private static final String DATASET = rb.getString("direccionExcepcionada.dataset"); 
	public static final String TABLE =  rb.getString("direccionExcepcionada.tabla");
	private static final String ALLFIELDS =  rb.getString("direccionExcepcionada.campos");
	public static final String GeoValidos = rb.getString("direccionExcepcionada.estadosGEOValidos");
	public static final String GeoNOValidos = rb.getString("direccionExcepcionada.estadosGEONOValidos");
	public static final String ComentarioProveedor = rb.getString("direccionExcepcionada.comentarioProveedor");
	
	public DireccionExcepcionada() {
		super();
	}

	
	public String getDetalleDireccion() {
		return detalleDireccion;
	}

	public void setDetalleDireccion(String detalleDireccion) {
		this.detalleDireccion = detalleDireccion;
	}

	public String getFechaIngresoDireccion() {
		return fechaIngresoDireccion;
	}

	public void setFechaIngresoDireccion(String fechaIngresoDireccion) {
		this.fechaIngresoDireccion = fechaIngresoDireccion;
	}

	public String getFechaEnvioProveedor() {
		return fechaEnvioProveedor;
	}

	public void setFechaEnvioProveedor(String fechaEnvioProveedor) {
		this.fechaEnvioProveedor = fechaEnvioProveedor;
	}

	public String getEstadoExcepcionType() {
		return estadoExcepcionType;
	}

	public void setEstadoExcepcionType(String estado_excepcion_type) {
		this.estadoExcepcionType = estado_excepcion_type;
	}

	public String getFechaRespuestaProveedor() {
		return fechaRespuestaProveedor;
	}

	public void setFechaRespuestaProveedor(String fecha_respuesta_proveedor) {
		this.fechaRespuestaProveedor = fecha_respuesta_proveedor;
	}

	public String getFechaEnvioSistema() {
		return fechaEnvioSistema;
	}

	public void setFechaEnvioSistema(String fecha_envio_sistema) {
		this.fechaEnvioSistema = fecha_envio_sistema;
	}

	public String getEstadoGestionType() {
		return estadoGestionType;
	}

	public void setEstadoGestionType(String estado_gestion_type) {
		this.estadoGestionType = estado_gestion_type;
	}

	public String getAutorizadaGestion() {
		return autorizadaGestion;
	}

	public void setAutorizadaGestion(String autorizada_gestion) {
		this.autorizadaGestion = autorizada_gestion;
	}

	public String getComentarioProveedor() {
		return comentarioProveedor;
	}

	public void setComentarioProveedor(String comentario_proveedor) {
		this.comentarioProveedor = comentario_proveedor;
	}

	public String getCodigoSolicitud() {
		return codigoSolicitud;
	}

	public void setCodigoSolicitud(String codigo_solicitud) {
		this.codigoSolicitud = codigo_solicitud;
	}

	public String getUsuarioSolicitud() {
		return usuarioSolicitud;
	}

	public void setUsuarioSolicitud(String usuario_solicitud) {
		this.usuarioSolicitud = usuario_solicitud;
	}

	public String getCodigoSistema() {
		return codigoSistema;
	}

	public void setCodigoSistema(String codigo_sistema) {
		this.codigoSistema = codigo_sistema;
	}

	public String getDireccionTextoLibre() {
		return direccionTextoLibre;
	}

	public void setDireccionTextoLibre(String direccion_texto_libre) {
		this.direccionTextoLibre = direccion_texto_libre;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(String telefonos) {
		this.telefonos = telefonos;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	
	public int getDirEnLotes() {
		return dirEnLotes;
	}

	public void setDirEnLotes(int dirEnLotes) {
		this.dirEnLotes = dirEnLotes;
	}

	public String getDireccionEstandarNLectura() {
		return direccionEstandarNLectura;
	}

	public void setDireccionEstandarNLectura(String direccionEstandarNLectura) {
		this.direccionEstandarNLectura = direccionEstandarNLectura;
	}

	public String getEstadoPagina() {
		return estadoPagina;
	}

	public void setEstadoPagina(String estadoPagina) {
		this.estadoPagina = estadoPagina;
	}

	public String getInstalacion() {
		return instalacion;
	}

	public void setInstalacion(String instalacion) {
		this.instalacion = instalacion;
	}

	public String getLoteId() {
		return loteId;
	}

	public void setLoteId(String loteId) {
		this.loteId = loteId;
	}

	public boolean esAutorizadaGestion(){
		return (getAutorizadaGestion()!=null && getAutorizadaGestion().trim().toLowerCase().equals("true"));
	}
	
	@Override
	public String obtenerDateSet() {
		return DATASET;
	}

	@Override
	public String obtenerTabla() {
		return TABLE;
	}

	@Override
	public void obtenerObjeto(String registros) throws GDEException {
		if (registros != null && !"0".equals(registros)){
			String[] registro = registros.split("\\|");
			for (int i=0; i < registro.length;i++){
				registro[i] = registro[i].replaceAll("[\\[\\]]", "");
				//Coloco ## para identificar los campos que vienen vac�os
				while(registro[i].indexOf(";;")>=0){
					registro[i] = registro[i].replaceAll(";;", ";##;");
				}
				if (registro[i].endsWith(";")) registro[i] = registro[i]+"##";
				
				String[] campos = registro[i].split(";");
				setFechaIngresoDireccion(quitarMascaraTexto(campos[0]));
				setFechaEnvioProveedor(quitarMascaraTexto(campos[1]));
				setEstadoExcepcionType(quitarMascaraTexto(campos[2]));
				setFechaRespuestaProveedor(quitarMascaraTexto(campos[3]));
				setFechaEnvioSistema(quitarMascaraTexto(campos[4]));
				setEstadoGestionType(quitarMascaraTexto(campos[5]));
				setAutorizadaGestion(quitarMascaraTexto(campos[6]));
				setComentarioProveedor(quitarMascaraTexto(campos[7]));
				setCodigoSolicitud(quitarMascaraTexto(campos[8]));
				setUsuarioSolicitud(quitarMascaraTexto(campos[9]));
				setCodigoSistema(quitarMascaraTexto(campos[10]));
				setCodigoDireccion(quitarMascaraTexto(campos[11]));
				setDireccionNormalizada(quitarMascaraTexto(campos[12]));
				setDireccionTextoLibre(quitarMascaraTexto(campos[13]));
				setMunicipio(quitarMascaraTexto(campos[14]));
				setDepartamento(quitarMascaraTexto(campos[15]));
				setContacto(quitarMascaraTexto(campos[16]));
				setTelefonos(quitarMascaraTexto(campos[17]));
				setEstadoGeorType(quitarMascaraTexto(campos[18]));
				setComentario(quitarMascaraTexto(campos[19]));
				setPais(quitarMascaraTexto(campos[20]));
				setRemanente("##".equals(campos[21])||" ".endsWith(campos[21])?null:campos[21]);
				setId(quitarMascaraEntero(campos[22]));
				setLoteId(quitarMascaraTexto(campos[23]));

			}
		}
		
	}

	
	/**
	 * M�todo que actualiza la direcci�n excepcionada y crea una versi�n
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void actualizarDirExcVersionLote() throws GDEException {
		
		try {
			// Se actualiza la informaci�n el la tabla direccion_excepcionada
			ejecutarServicioActualizarElementoTabla("codigo_solicitud", getCodigoSolicitud(), "direccion_normalizada|estado_excepcion_type|estado_geor_type|comentario_proveedor", getDireccionNormalizada()+"|"+getEstadoExcepcionType()+"|"+getEstadoGeorType()+"|"+getComentarioProveedor());
		} catch (GDEException e) { 
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
		// Se configura el ingreso de la versi�n
		VersionDireccionExcepcionada verDirExec = new VersionDireccionExcepcionada();
		
		// Se le da formato a la fecha de ingreso
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date dateIngreso = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(getFechaIngresoDireccion());
			verDirExec.setFechaIngresoDireccion(sdf.format(dateIngreso));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		// Se asignan los valores
		verDirExec.setEstadoGestionType(getEstadoGestionType());
		verDirExec.setEstadoExcepcion(getEstadoExcepcionType());
		verDirExec.setEstadoGeoreferenciacion(getEstadoGeorType());
		//Obtengo la hora del sistema
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        //Coloco el formato de hora y minutos
        
		verDirExec.setFechaModificacion(sdf.format(now));
		verDirExec.setFechaEnvioSistema(getFechaEnvioSistema());
		verDirExec.setFechaEnvioProveedor(getFechaEnvioProveedor());
		verDirExec.setDireccionExcepcionadaId(getId());
		verDirExec.setNombreArchivo(" ");
		
		// Se inserta la versi�n de la direcci�n excepcionada
		verDirExec.insertarVerDirExec();
	
		
	}

	/**
	 * Funci�n que invoca el servicio de RespuestaDE
	 * @return
	 * 		verdadero si se ingres� la direcci�n excepcionada en CRM
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public boolean reportarDE() throws GDEException {
		
		boolean completado = false;
		try {
			WSRespuestaDEStub proxy = new WSRespuestaDEStub();
			WSRespuestaDEStub.WSRespuestaDERQ request = new WSRespuestaDEStub.WSRespuestaDERQ();
			WSRespuestaDEStub.WSRespuestaDERS response = null;
			
			request.setWSRespuestaDERQ(new WSRespuestaDEStub.WSRespuestaDERQType());
			WSRespuestaDEStub.BoundedString100 agregado100,codigoDireccion100,codigoDireccionProveevor100,nombreBarrio100,nombreComuna100,nombreLocalizacionTipo1100,remanente100 = null;
			WSRespuestaDEStub.BoundedString10 codigoBarrio10, codigoComuna10,codigoLocalizacionTipo110,placa10 = null;
			WSRespuestaDEStub.BoundedString14 codigoDaneManzana14 = null;
			WSRespuestaDEStub.BoundedString15 codigoSolicitud15= null;
			WSRespuestaDEStub.BoundedString5 estadoPagina5= null;
			WSRespuestaDEStub.BoundedString1 estadoGeoreferenciacion1 =  null;
			WSRespuestaDEStub.BoundedString250 direccionAnterior250,direccionEstandarNLectura250,direccionNormalizada250= null;
			WSRespuestaDEStub.BoundedString30 codigoSistema30,estadoExcepcion30 = null;
			WSRespuestaDEStub.BoundedString17 codigoPredio17 = null;
			WSRespuestaDEStub.BoundedString8 codigoMunicipio8= null;
			WSRespuestaDEStub.BoundedString18 instalacion18= null;
			WSRespuestaDEStub.BoundedString2 codigoDepartamento2, codigoPais2,rural2 = null;
			WSRespuestaDEStub.BoundedString20 tipoAgregacionNivel120 = null;
			
			tipoAgregacionNivel120  =  new WSRespuestaDEStub.BoundedString20();
			tipoAgregacionNivel120.setBoundedString20(UtilGDE.colocarMascara(getTipoAgregacionNivel1()));
			rural2 =  new WSRespuestaDEStub.BoundedString2();
			rural2.setBoundedString2(UtilGDE.colocarMascara(getRural()));
			remanente100 = new WSRespuestaDEStub.BoundedString100();
			remanente100.setBoundedString100(UtilGDE.colocarMascara(getRemanente()));
			placa10= new WSRespuestaDEStub.BoundedString10();
			placa10.setBoundedString10(UtilGDE.colocarMascara(getPlaca()));
			nombreLocalizacionTipo1100= new WSRespuestaDEStub.BoundedString100();
			nombreLocalizacionTipo1100.setBoundedString100(UtilGDE.colocarMascara(getNombreLocalizacionTipo1()));
			nombreComuna100 = new WSRespuestaDEStub.BoundedString100();
			nombreComuna100.setBoundedString100(UtilGDE.colocarMascara(getNombreComuna()));
			nombreBarrio100 = new WSRespuestaDEStub.BoundedString100();
			nombreBarrio100.setBoundedString100(UtilGDE.colocarMascara(getNombreBarrio()));
			instalacion18= new WSRespuestaDEStub.BoundedString18();
			instalacion18.setBoundedString18(UtilGDE.colocarMascara(getInstalacion()));
			estadoPagina5= new WSRespuestaDEStub.BoundedString5();
			estadoPagina5.setBoundedString5(UtilGDE.colocarMascara(getEstadoPagina()));
			estadoGeoreferenciacion1= new WSRespuestaDEStub.BoundedString1();
			estadoGeoreferenciacion1.setBoundedString1(UtilGDE.colocarMascara(getEstadoGeorType()));
			estadoExcepcion30= new WSRespuestaDEStub.BoundedString30(); 
			estadoExcepcion30.setBoundedString30(UtilGDE.colocarMascara(getEstadoExcepcionType()));
			direccionNormalizada250 = new WSRespuestaDEStub.BoundedString250();
			direccionNormalizada250.setBoundedString250(UtilGDE.colocarMascara(getDireccionNormalizada()));
			direccionEstandarNLectura250 = new WSRespuestaDEStub.BoundedString250();
			direccionEstandarNLectura250.setBoundedString250(UtilGDE.colocarMascara(getDireccionEstandarNLectura()));
			direccionAnterior250 = new WSRespuestaDEStub.BoundedString250();
			direccionAnterior250.setBoundedString250(UtilGDE.colocarMascara(getDireccionAnterior()));
			codigoSolicitud15  = new WSRespuestaDEStub.BoundedString15();
			codigoSolicitud15.setBoundedString15(UtilGDE.colocarMascara(getCodigoSolicitud()));
			codigoSistema30  = new WSRespuestaDEStub.BoundedString30(); 
			codigoSistema30.setBoundedString30(UtilGDE.colocarMascara(getCodigoSistema()));
			codigoPredio17 = new WSRespuestaDEStub.BoundedString17(); 
			codigoPredio17.setBoundedString17(UtilGDE.colocarMascara(getCodigoPredio()));
			codigoMunicipio8 = new WSRespuestaDEStub.BoundedString8(); 
			codigoMunicipio8.setBoundedString8(UtilGDE.colocarMascara(getMunicipio()));
			codigoDireccionProveevor100 = new WSRespuestaDEStub.BoundedString100();
			codigoDireccionProveevor100.setBoundedString100(UtilGDE.colocarMascara(getCodigoDireccionProveevor()));
			agregado100 = new WSRespuestaDEStub.BoundedString100();
			agregado100.setBoundedString100(UtilGDE.colocarMascara(getAgregado()));
			codigoDireccion100 = new WSRespuestaDEStub.BoundedString100();
			codigoDireccion100.setBoundedString100(UtilGDE.colocarMascara(getCodigoDireccion()));
			codigoComuna10 = new WSRespuestaDEStub.BoundedString10();
			codigoBarrio10 = new WSRespuestaDEStub.BoundedString10();
			codigoLocalizacionTipo110 = new WSRespuestaDEStub.BoundedString10();
			codigoLocalizacionTipo110.setBoundedString10(UtilGDE.colocarMascara(getCodigoLocalizacionTipo1()));
			codigoPais2 = new WSRespuestaDEStub.BoundedString2();
			codigoPais2.setBoundedString2(UtilGDE.colocarMascara(getPais()));
			codigoDepartamento2 = new WSRespuestaDEStub.BoundedString2();
			codigoDepartamento2.setBoundedString2(UtilGDE.colocarMascara(getDepartamento()));
			codigoDaneManzana14  = new WSRespuestaDEStub.BoundedString14();
			codigoDaneManzana14.setBoundedString14(UtilGDE.colocarMascara(getCodigoDaneManzana()));
			codigoBarrio10.setBoundedString10(UtilGDE.colocarMascara(getCodigoBarrio()));
			codigoComuna10.setBoundedString10(UtilGDE.colocarMascara(getCodigoComuna()));
			
			request.getWSRespuestaDERQ().setAgregado(agregado100);
			request.getWSRespuestaDERQ().setCodigoBarrio(codigoBarrio10);
			request.getWSRespuestaDERQ().setCodigoComuna(codigoComuna10);
			request.getWSRespuestaDERQ().setCodigoDaneManzana(codigoDaneManzana14);
			request.getWSRespuestaDERQ().setCodigoDireccion(codigoDireccion100);
			request.getWSRespuestaDERQ().setCodigoDepartamento(codigoDepartamento2);
			request.getWSRespuestaDERQ().setCodigoDireccionProveevor(codigoDireccionProveevor100);
			request.getWSRespuestaDERQ().setCodigoLocalizacionTipo1(codigoLocalizacionTipo110);
			request.getWSRespuestaDERQ().setCodigoMunicipio(codigoMunicipio8);
			request.getWSRespuestaDERQ().setCodigoPais(codigoPais2);
			request.getWSRespuestaDERQ().setCodigoPredio(codigoPredio17);
			request.getWSRespuestaDERQ().setCodigoSistema(codigoSistema30);
			request.getWSRespuestaDERQ().setCodigoSolicitud(codigoSolicitud15);
			request.getWSRespuestaDERQ().setCoordenadaX(getCoordenadaX());
			request.getWSRespuestaDERQ().setCoordenadaY(getCoordenadaY());
			request.getWSRespuestaDERQ().setDireccionAnterior(direccionAnterior250);
			request.getWSRespuestaDERQ().setDireccionEstandarNLectura(direccionEstandarNLectura250);
			request.getWSRespuestaDERQ().setDireccionNormalizada(direccionNormalizada250);
			request.getWSRespuestaDERQ().setEstadoExcepcion(estadoExcepcion30);
			request.getWSRespuestaDERQ().setEstadoGeoreferenciacion(estadoGeoreferenciacion1);
			request.getWSRespuestaDERQ().setEstadoPagina(estadoPagina5);
			request.getWSRespuestaDERQ().setEstrato(getEstrato());
			request.getWSRespuestaDERQ().setInstalacion(instalacion18);
			request.getWSRespuestaDERQ().setLatitud(getLatitud());
			request.getWSRespuestaDERQ().setLongitud(getLongitud());
			request.getWSRespuestaDERQ().setNombreBarrio(nombreBarrio100);
			request.getWSRespuestaDERQ().setNombreComuna(nombreComuna100);
			request.getWSRespuestaDERQ().setNombreLocalizacionTipo1(nombreLocalizacionTipo1100);
			request.getWSRespuestaDERQ().setPlaca(placa10);
			request.getWSRespuestaDERQ().setRemanente(remanente100);
			request.getWSRespuestaDERQ().setRural(rural2);
			request.getWSRespuestaDERQ().setTipoAgregacionNivel1(tipoAgregacionNivel120);
			
		
			long timeout = 2 * 60 * 1000; // Two minutes

			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);
			
			response = proxy.respuestaDE(request);

			completado = "OK".equals(response.getWSRespuestaDERS().getGisRespuestaProceso().getCodigoRespuesta());
			
			if (!completado &&  !"".equals(response.getWSRespuestaDERS().getGisRespuestaProceso().getDescripcionError())){
				String errorTecnico = response.getWSRespuestaDERS().getGisRespuestaProceso().getDescripcionError()+" ("+response.getWSRespuestaDERS().getGisRespuestaProceso().getCodigoError()+")";
				GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServResDEException,errorTecnico));
				throw gdeException;
			}
			
			
		} catch (AxisFault e) {
			GDEException gdeException = new GDEException(messageServResDEException);
			e.printStackTrace();
			throw gdeException;
		} catch (RemoteException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(errorTecnicoServResDEException,e.getMessage()));
			e.printStackTrace();
			throw gdeException;
		}
		return completado;
	}
	
	/**
	 * M�todo que actualiza el estado de gesti�n de la direcci�n excepcionada. Se tiene que tener cargado el c�digo de direcci�n
	 * @param estadoGestionType
	 * 			Estado de gesti�n
	 * @throws GDEException
	 */
	public void actualizarEstadoGestionType(String estadoGestionType) throws GDEException {
		try {
			ejecutarServicioActualizarEstadoDirExc(getCodigoDireccion(), estadoGestionType);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		try {
			ejecutarServicioCrearVersionDirExc(getCodigoDireccion(),"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
	}

	/**
	 * M�todo que relaciona la direcci�n excepcionada con un lote
	 * @param idLote
	 * 			Id del lote
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void relacionarLote(int idLote) throws GDEException {
		
		try {
			ejecutarServicioRelacionarElementoTabla(LoteDireccionExcepcionada.TABLE, getId()+"", idLote+"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}

	/**
	 * M�todo que relaciona la direcci�n Excepcionada con detalle_direccion.
	 * @param codigoDireccion
	 * 			C�digo de direcci�n
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void relacionarDireccionGeo (String codigoDireccion) throws GDEException {
		
		try {
			ejecutarServicioRelacionarElementoTabla(Direccion.TABLE, getId()+"", codigoDireccion+"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}
	
	/**
	 * M�todo que relaciona la direcci�n Excepcionada con detalle_direccion. Se tiene que tener cargado el c�digo de direcci�n
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void relacionarDireccionGeo () throws GDEException {
		
		try {
			ejecutarServicioRelacionarElementoTabla(Direccion.TABLE, getId()+"", getCodigoDireccion());
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}

	/**
	 * M�todo que permite obtener una direcci�n excepcionda
	 * @param codigoSolicitud
	 * 			C�digo de solicitud
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void obtenerDirExcPorCodigoSolicitud(String codigoSolicitud) throws GDEException {
		try {
			ejecutarServicioConsultaTabla(ALLFIELDS, "codigo_solicitud", codigoSolicitud);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}

	/**
	 * M�todo que permite obtener una direcci�n excepcionda
	 * @param id
	 * 			id de la direcci� excepcionda
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void obtenerDirExcPorId(String id) throws GDEException {
		try {
			ejecutarServicioConsultaTabla(ALLFIELDS, "id", id);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}

	/**
	 * M�todo que elimina una direcci�n excepcionda de la BD
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void eliminarDirExc() throws GDEException {
		try {
			ejecutarServicioEliminarElementoTabla("id", getId()+"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
	}
	
	/**
	 * M�todo que guarda una versi�n de la direcci�n excepcionada luego de ser guardada
	 * @param nombreCampos
	 * 			listado de nombres de los campos a modificar
	 * @param valoresCampos
	 * 			listado de los valores a mofidicar
	 * @param nombreArchivo
	 * 			nombre de archivo del lote. Puede ser null
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void actualizarDirExcVersionada(String nombreCampos, String valoresCampos, String nombreArchivo) throws GDEException{
		try {
			// Se actualiza la informaci�n en direcci�n Excepcionada
			ejecutarServicioActualizarElementoTabla("id", getId()+"", nombreCampos, valoresCampos);
		} catch (GDEException e) { 
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
		try {
			
			// Se carga los datos modificados en el objeto
			ejecutarServicioConsultaTabla(ALLFIELDS, "id", getId()+"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
		try {
			
			// Se guarda la nueva versi�n 
			ejecutarServicioCrearVersionDirExc(getCodigoDireccion(),nombreArchivo);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
	}

}

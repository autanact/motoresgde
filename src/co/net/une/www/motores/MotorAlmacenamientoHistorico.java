package co.net.une.www.motores;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: MotorAlmacenamientoHistorico.java
##	Contenido: Clase que implementa el motor de Almacenamiento de Hist�rico
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Creaci�n de la clase
##**********************************************************************************************************************************
*/

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import co.net.une.www.bean.DireccionExcepcionada;
import co.net.une.www.bean.DireccionExcepcionadaList;
import co.net.une.www.bean.EnvioDireccionHistorico;
import co.net.une.www.bean.HistoricoDireccionExcepcionada;
import co.net.une.www.bean.HistoricoVersionDireccionExcepcionada;
import co.net.une.www.bean.LoteDireccionExcepcionada;
import co.net.une.www.bean.VersionDireccionExcepcionada;
import co.net.une.www.bean.VersionDireccionExcepcionadaList;
import co.net.une.www.util.GDEException;
import co.net.une.www.util.UtilGDE;
/**
 * Clase que implementa el motor de Almacenamiento de Hist�rico
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class MotorAlmacenamientoHistorico {

	
	/**
	 * M�todo que permite ejecutar el motor desde un Shell
	 * @param args
	 */
	public static void main(String[] args) {
		
		// ejecuta el motor
		ejecutarMotor();

	}

	/**
	 * M�todo que ejecuta las funciones del motor de Almacenamiento de hist�ricos
	 */
	public static void ejecutarMotor(){
		try {
			System.out.println("Inicia motor de almacenamiento de historicos");
			EnvioDireccionHistorico envioDirExec = new EnvioDireccionHistorico();
			
			envioDirExec.obtenerEnvioDireccionHistorico();
			if (envioDirExec.diaEjecucion()) {

				//System.out.println("Se ejecuta el dia de hoy");
				//Se ejecuta el d�a de hoy
				DireccionExcepcionadaList dirExcList = new DireccionExcepcionadaList();
				//obtengo los c�digo de solicitud a migrar
				dirExcList.obtenerListaIdDirExcParaHistorico();
					
				//System.out.println("Se van a revisar: "+dirExcList.totalDirecciones()+" direcciones");

				Iterator<DireccionExcepcionada> dirExcIterator = dirExcList.getDirExcList().iterator();

				while(dirExcIterator.hasNext()){ 
					DireccionExcepcionada dirExc = dirExcIterator.next();
						
					dirExc.obtenerDirExcPorId(dirExc.getId()+"");
								
					//Convierto a fecha con formato
					Date fechaIngDir = UtilGDE.convertirObjetoFecha("dd/MM/yyyy", dirExc.getFechaIngresoDireccion());
					Date fechaIniHist = UtilGDE.convertirObjetoFecha("dd/MM/yyyy", envioDirExec.getFechaInicialHistorico());
					
					//La fecha de inicio del historico, tiene que ser tantos d�as antes como los configurados
					Calendar c = Calendar.getInstance();
					c.setTime(fechaIniHist);
					c.add(Calendar.DATE, envioDirExec.getDias()*(-1)); 
					fechaIniHist = c.getTime();
					
					//Se autoriza la gesti�n de la direcci�n excepcionada
					if (fechaIngDir.before(fechaIniHist) && 
							(
								("false".equals(dirExc.getAutorizadaGestion().toLowerCase()))||
								("true".equals(dirExc.getAutorizadaGestion().toLowerCase())) && 
									(
										(
											("PUBLICADA CRM".equals(dirExc.getEstadoGestionType())) && 
											(
												("EXACTA".equals(dirExc.getEstadoExcepcionType())) || 
												("RURAL".equals(dirExc.getEstadoExcepcionType()))
											)
										) ||
										(
											("RESUELTA PROVEEDOR".equals(dirExc.getEstadoGestionType()) && 
													"EXACTA".equals(dirExc.getEstadoExcepcionType())&& "MIG".equals(dirExc.getCodigoSistema())
											)
										)
									)
								)
							){
							
						//Se ubica la direcci�n en el lote
							
						//System.out.println("Migro dirExc:"+dirExc.getId());
						LoteDireccionExcepcionada loteDirExc = new LoteDireccionExcepcionada();
						if (dirExc.getLoteId()!=null)
							loteDirExc.obtenerLotePorIdLote(dirExc.getLoteId());
						
						HistoricoDireccionExcepcionada histDirExc = new HistoricoDireccionExcepcionada();
						
						histDirExc.migrarDireccionExcepcionada(dirExc);
						
						// Asigno el nombre del archivo
						histDirExc.setNombreArchivo(loteDirExc.getNombreArchivo());
						
						// Ingreso en el historial del lote
						//System.out.println("Inserto la direcci�n en Historico");
						histDirExc.insertarHistoricoDirExc();
						
						//Busco los registros en Versi�n Direcci�n Excepcionada Lote
						
						VersionDireccionExcepcionadaList verDirExcList = new VersionDireccionExcepcionadaList();
						
						verDirExcList.obtenerListaIdVerDirExcPorDirExcId(dirExc.getId()+"");
						
						//System.out.println("Obtengo las "+verDirExcList.totalVerDirExcLote() +" versiones");
						if (verDirExcList.totalVerDirExcLote() >0) {
							
							Iterator<VersionDireccionExcepcionada> VerDirExcLTIterator = verDirExcList.getVerDirExcLoteList().iterator();
							while(VerDirExcLTIterator.hasNext()){
								VersionDireccionExcepcionada verDirExcId = VerDirExcLTIterator.next();
								VersionDireccionExcepcionada verDirExc = new VersionDireccionExcepcionada();
								HistoricoVersionDireccionExcepcionada histVerDirExc = new HistoricoVersionDireccionExcepcionada();
								verDirExc.obtenerVerDirExcPorId(verDirExcId.getId());
								histVerDirExc.migrarVersionDireccionExcepcionada(verDirExc);
								histVerDirExc.setIdHistDirExc(histDirExc.getId());
								//System.out.println("Inserto Versi�n");
								histVerDirExc.insertarHistVerDirExc();
								verDirExc.eliminarVerDirExc();
								
								//System.out.println("Elimino VersionDirExc: "+verDirExcId.getId());
							}
							
						}
						dirExc.eliminarDirExc();
						//System.out.println("Elimino DirExc: "+dirExc.getId());
					}
				}
				envioDirExec.actualizarProximaEjecucion();
			}else{
				//System.out.println("No me ejecuto hoy");
			}
			
			System.out.println("Fin almacenamiento de historicos");
		} catch (GDEException e) {
			System.out.println(e.getMensage());
		}
		
	}
}

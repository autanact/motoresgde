package co.net.une.www.motores;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: GeneradorLotesTimerTask.java
##	Contenido: Clase que implementa la tarea de ejecuci�n del motor de generaci�n de lotes
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Creaci�n de la clase
##**********************************************************************************************************************************
*/
import java.io.File;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;

import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.bean.LoteDireccionExcepcionada;
import co.net.une.www.bean.PeridiocidadDirecciones;
import co.net.une.www.util.GDEException;
import co.net.une.www.util.UtilGDE;
import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;


/**
 * Clase que implementa la tarea de ejecuci�n del motor de generaci�n de lotes
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class GeneradorLotesTimerTask extends TimerTask {
	
	//archivo de propiedades
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.MotorGeneradorLotes", Locale.getDefault());
	
	//archivo de configuraci�n
	private static final String configFilePath = ServiciosUtil.getMotorLoteConfigFile();
	private static final String nombreAmbiente = ServiciosUtil.getNombreAmbiente();
	
	//errores
	private static final String directorioNoExiste = rb.getString("error.archivo.directorioNoExiste");
	private static final String directorioErrorLectura = rb.getString("error.archivo.directorioErrorLectura");
	private static final String messageGDEError = rb.getString("error.messageGDEError");
	
	
	private int periodicidad = 0;	
	private String usuario = null;
	private String pathSave = null;
	
	private ServletContext servletContext;
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPathSave() {
		return pathSave;
	}

	public void setPathSave(String pathSave) {
		this.pathSave = pathSave;
	}

	public GeneradorLotesTimerTask(String usu,String path,ServletContext servletContext) {

		setPeriodicidad(0);
		this.setUsuario(usu);
		this.setPathSave(path);
		this.setServletContext(servletContext);
		System.out.println("Se configura en motor GeneradorLotesTimerTask en: "+nombreAmbiente);
	}

	public GeneradorLotesTimerTask(int periodicidad,String usu,String path) {
		super();
		this.setPeriodicidad(periodicidad);
		this.setUsuario(usu);
		this.setPathSave(path);
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public int getPeriodicidad() {
		return periodicidad;
	}

	public void setPeriodicidad(int periodicidad) {
		this.periodicidad = periodicidad;
	}

	public void run()
    {
		System.out.println("**********************");
		System.out.println("Inicia motor GeneradorLotesTimerTask:"+getPeriodicidad());
		
		int periAux = getPeriodicidad();
		
        //Coloco el formato de hora y minutos para la hora actual
        Calendar cal = Calendar.getInstance();
        Date defaultDate = new Date();
        cal.setTime(defaultDate);
        
		try {
			File directorio = new File(configFilePath);
			
			//Se verifica que el directorio donde se van a guardar los lotes existan.
			if (!directorio.isDirectory()){
				GDEException gdeException = new GDEException(MessageFormat.format(directorioNoExiste, configFilePath));
				throw gdeException;
			}
			
			//Se verifica que el directorio donde se van a guardar los lotes enga 
			if (!directorio.canWrite()){
				GDEException gdeException = new GDEException(MessageFormat.format(directorioErrorLectura, configFilePath));
				throw gdeException;
			}
			
			PeridiocidadDirecciones periocidadDirecciones = new PeridiocidadDirecciones();
			
			//Obtengo la hora del sistema
	        Calendar cal1 = Calendar.getInstance();
	        Date now = new Date();
	        cal.setTime(now);
	        
			//Coloco el formato de hora y minutos
	        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
	        String horaAct = sdf.format(now);
	        
	        //Obtengo el dia de la semana
	        int diaAct = cal1.get(Calendar.DAY_OF_WEEK); 	        
	        
	        //Obtengo las configuraci�n de direcciones
			periocidadDirecciones.obtenerPeriodicidadDirecciones();
			
			//obtengo la hora final del proceso
		    Date horaFin = UtilGDE.convertirObjetoFecha("HHmm",periocidadDirecciones.getHoraFinal() );
		    
		    int repeticion = Integer.parseInt((String) getServletContext().getAttribute("motorLoteRep"));
		    
		    if (repeticion == getPeriodicidad()) {
		    	
		    	// Valido que soy el unico que estoy funcionando
		    	
				//Valido que el proceso est� dentro del rango de avisar
				if (!periocidadDirecciones.estaEnRango(diaAct, horaAct)) {
					
					//Estoy fuera del rango para obtener la pr�xima fecha para la ejecuci�n
		        	defaultDate = obtenerProximaFecha(UtilGDE.convertirObjetoFecha("HHmm",horaAct), horaFin, periocidadDirecciones.getHoraInicial());
				}else {
					
		        	//Estoy dentro del rango
					
					try {
						LoteDireccionExcepcionada.publicarLote(getUsuario(), getPathSave());
					}catch (Exception e){}	
	
		        	//Se vuelve a agendar a la pr�xima ejecuci�n dependiendo de la periodicidad en minutos.
		        	Date ahora = new Date();
		        	Calendar c1 = Calendar.getInstance();
		        	c1.setTime(ahora);
		        	// La periodicidad se calcula en a partir de horas y minutos
		        	String horaPer = periocidadDirecciones.getPeriodicidad().substring(0,2);
		        	int minCur = Integer.parseInt(horaPer)*60;
		        	String minPer = periocidadDirecciones.getPeriodicidad().substring(2,4);
		        	minCur = minCur+Integer.parseInt(minPer);
		        	if (minCur==0)
		        		minCur=60*24;
		        	c1.add(Calendar.MINUTE,minCur);
		        	defaultDate = c1.getTime();
		        }
				
				//Configurar pr�xima ejecuci�n del servicio
			    configurarProximaEjecucion(defaultDate,periAux);
		    }else{
		    	//System.out.println("Hay un proceso m�s nuevo de Lotes ME MUERO");
		    }
		    
		} catch (GDEException e) {
			System.out.println(messageGDEError+" "+e.getMensage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(messageGDEError+" "+e.getMessage());
			e.printStackTrace();
		}
		
    }

	/**
	 * M�todo para agendar la pr�xima corrida de la ejecuci�n
	 * @param defaultDate
	 * 			Fecha del nuevo proceso
	 * @param periAux
	 * 			Repitici�n del proceso
	 */
	private void configurarProximaEjecucion(Date defaultDate, int periAux) {
		//obtengo la hora actual
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date now = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		
		Calendar c1 = Calendar.getInstance();
		c1.setTime(defaultDate);
				
		//Inicializo las variales
		Timer timer = new Timer();
		int nextRep = periAux+1;
		GeneradorLotesTimerTask inicio = new GeneradorLotesTimerTask(getUsuario(),getPathSave(),getServletContext());
		inicio.setPeriodicidad(nextRep);
		inicio.getServletContext().removeAttribute("motorLoteRep");
		inicio.getServletContext().setAttribute("motorLoteRep", nextRep+"");
			    
		//Muestro la hora actual
		System.out.println("**********************");
		System.out.println("Hora Actual: "+ sdf.format(now.getTime()));	 
			    
		//Muestro la ejecuci�n de la tarea     
		System.out.println("Pr�xima hora de ejecuci�n motor GeneradorLotesTimerTask: "+sdf.format(c1.getTime()));
		System.out.println("**********************");
			    
		//Agendo el proceso con el nuevo tiempo
		timer.schedule(inicio, c1.getTime());
		
	}

	/**
	 * M�todo que se utiliza para obtener la pr�xima fecha
	 * @param defaultDate
	 * 			Fecha actual
	 * @param horaFin
	 * 			Fecha fin del rango de fecha
	 * @param horaInicio
	 * 			Fecha inicio del rango de fecha
	 * @return
	 * 			Nueva fecha
	 * @throws GDEException
	 */
	private Date obtenerProximaFecha(Date defaultDate, Date horaFin, String horaInicio) throws GDEException{
		//Le coloco le fecha inicial
		//Veo si la ejecuci�n termin� el mismo d�as
		boolean mismoDia = (TimeUnit.DAYS.convert(defaultDate.getTime()-horaFin.getTime(), TimeUnit.MILLISECONDS)==0);
		
		//verifico si todav�a la hora final est� en el mismo d�a
		Calendar c1 = Calendar.getInstance();
		Date hoy = new Date();
		c1.setTime(hoy);
		String nextHour = c1.get(Calendar.DAY_OF_MONTH)+"/"+(c1.get(Calendar.MONTH)+1)+"/"+c1.get(Calendar.YEAR)+" "+horaInicio;
		c1.setTime(UtilGDE.convertirObjetoFecha("dd/MM/yyyy HHmm", nextHour));
		if (mismoDia){
			
			//es el mismo dia, la pr�xima ejecuci�n ser� al d�a siguiente a la hora de inicio
			c1.add(Calendar.DATE,1);
		}
		
		return c1.getTime();
	}

}

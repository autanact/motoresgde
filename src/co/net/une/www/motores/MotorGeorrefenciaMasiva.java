package co.net.une.www.motores;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: MotorGeorrefenciaMasiva.java
##	Contenido: Clase que implementa el motor de georreferencia masiva
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Creaci�n de la clase
##**********************************************************************************************************************************
*/
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.bean.Direccion;
import co.net.une.www.bean.DireccionExcepcionada;
import co.net.une.www.bean.DireccionExcepcionadaList;
import co.net.une.www.util.GDEException;
import co.net.une.www.util.UtilGDE;
/**
 * Clase que implementa el motor de georreferencia masiva
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class MotorGeorrefenciaMasiva {

	//archivo de propiedades
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.MotorGeorrefenciaMasiva", Locale.getDefault());
	//archivo de configuraci�n
	private static final String configFilePath =  ServiciosUtil.getMotorGeoConfigFile();
	//errores
	private static final String faltanParametros = rb.getString("error.parametros.faltanParametros");
	private static final String faltaConfiguracion = rb.getString("error.archivo.faltaConfiguracion");
	private static final String archivoNoExiste = rb.getString("error.archivo.archivoNoExiste");
	private static final String archivoErrorLectura = rb.getString("error.archivo.archivoErrorLectura");
	private static final String messageGDEError = rb.getString("error.messageGDEError");
	
	/**
	 * M�todo que permite ejecutar la motor desde un shell
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Inicia motor de MotorGeorrefenciaMasiva");
		try{
			if ((args!= null)  && (args.length==1)){
				ejecutarMotor();
			}else{
				System.out.println(messageGDEError+" "+faltanParametros);
			}
		} catch (Exception e1) {
			System.out.println(messageGDEError+" "+e1.getMessage());
			e1.printStackTrace();
		}	
		System.out.println("Termin� la ejecuci�n del motor");
	}
	

	
	/**
	 * Funci�n que obtiene el valor de un par�metro desde un string
	 * @param linea
	 * 			L�nea a evaluar
	 * @param nombreParametro
	 * 			nombre de par�metro a ubicar
	 * @return
	 * 		valor del par�metro
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public static String obtenerParametroArchivo(String linea, String nombreParametro) throws GDEException{
		if (linea!=null){
			String [] lineArr = linea.split("\\:");
		    if (lineArr.length == 2) {
		    	return  lineArr[1];
		    }else{
		    	GDEException gdeException = new GDEException(MessageFormat.format(faltaConfiguracion, nombreParametro));
				throw gdeException;
		    }
		 }else{
		    	GDEException gdeException = new GDEException(MessageFormat.format(faltaConfiguracion, nombreParametro));
				throw gdeException;
		 }
	}
	
	/**
	 * Funci�n que indica si la hora ingresada est� dentro del rango establecido
	 * @param horaActual
	 * 			hora a evaluar
	 * @param horaInicio
	 * 			hora inicial del rango
	 * @param horaFinal
	 * 			hora final del rango
	 * @return
	 * 		verdadero la hora ingresada est� dentro del rango establecido
	 * @throws GDEException
	 */
	public static boolean estaEnRango(Date horaActual, Date horaInicio, Date horaFinal) throws GDEException{
		boolean enRango = false;
		if (horaFinal.before(horaInicio)){
			Date medNoche =  UtilGDE.convertirObjetoFecha("HHmm","2359");
			Date medNoche1 = UtilGDE.convertirObjetoFecha("HHmm","0000");
			enRango= (horaActual.after(horaInicio) && horaActual.before(medNoche)) || (horaActual.after(medNoche1) && horaActual.before(horaFinal)) ;
		}else{
			enRango = horaActual.after(horaInicio) && horaActual.before(horaFinal) ;
		}
		return enRango;
	}



	/**
	 * M�todo que ejecuta el procesamiento de georreferencia masiva
	 */
	public static void ejecutarMotor() {
		System.out.println("Inicia motor MotorGeorrefencia: ");
		
        //Coloco el formato de hora y minutos para la hora actual
        Calendar cal = Calendar.getInstance();
        Date defaultDate = new Date();
        cal.setTime(defaultDate);
		
        Properties propiedades = null;
        InputStream input = null;
		//BufferedReader br = null;
		try {
			
			//formateo la fecha por defecto
	        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
	        String horaAct = sdf.format(defaultDate);
	        defaultDate = UtilGDE.convertirObjetoFecha("HHmm", horaAct);
	        
			//Leo el archivo de propiedades
	        input = new FileInputStream(configFilePath);
	        
			//cargo la informaci�n desde el archivo de propiedades
	        propiedades = new Properties();
			propiedades.load(input);
			//br = new BufferedReader(new FileReader(configFilePath));
		    //String line = br.readLine();
		    
		    //obtengo las variables del archivo de propiedades
		    //obtengo la hora de inicio de proceso
		    String horaInicio =propiedades.getProperty("hora_inicio");
		    Date horaIni = UtilGDE.convertirObjetoFecha("HHmm", horaInicio);
		    //line = br.readLine();
		    //obtengo la hora final del proceso
		    String horaFinal = propiedades.getProperty("hora_fin");
		    Date horaFin = UtilGDE.convertirObjetoFecha("HHmm", horaFinal);
		    //line = br.readLine();
		    
		    //Coloco el formato de hora y minutos para la hora actual
	        Date now = new Date();
	        cal.setTime(now);
	       
	        now = UtilGDE.convertirObjetoFecha("HHmm", horaAct);
	        
	        DireccionExcepcionadaList dirExcList = new DireccionExcepcionadaList();
	        dirExcList.obtenerListaIdDirExcSegunCriterio("estado_excepcion_type", "PARCIAL");
	        //System.out.println("Voy a georeferenciar "+dirExcList.totalDirecciones()+" direcciones");
	        if (dirExcList.totalDirecciones() > 0){
	        	Iterator<DireccionExcepcionada> dirExcIterator = dirExcList.getDirExcList().iterator();
				while(dirExcIterator.hasNext() && estaEnRango(now, horaIni, horaFin)){ 
					
					DireccionExcepcionada dirExc = dirExcIterator.next();
					dirExc.obtenerDirExcPorId(dirExc.getId()+"");
					Direccion dir = new Direccion();
					dir.obtenerDireccionGeoreferenciada(dirExc.getPais(),dirExc.getDepartamento(),dirExc.getMunicipio(),dirExc.getDireccionNormalizada());
					if (dir.getEstadoGeorType()!=dirExc.getEstadoGeorType()){
						//Cambia el estado de georeferencia
						if (DireccionExcepcionada.GeoValidos.indexOf(dir.getEstadoGeorType())>=0 ){
							dirExc.actualizarEstadoGestionType("EXACTA");
						}
						
					}
					
					//Obtengo la nueva hora
					 now = new Date();
				     cal.setTime(now);
				     horaAct = ( new SimpleDateFormat("HHmm")).format(now);
				     now = UtilGDE.convertirObjetoFecha("HHmm", horaAct);
	        	}
	        }
	        
		    
		} catch (FileNotFoundException e) {
			//No se encontr� el archivo de configuraci�n
			System.out.println(MessageFormat.format(archivoNoExiste,configFilePath));
			
		} catch (IOException e) {
			//No se pudo leer el archivo de configuraci�n
			System.out.println(MessageFormat.format(archivoErrorLectura, configFilePath));

		} catch (GDEException e) {
			System.out.println(messageGDEError+" "+e.getMensage());
			e.printStackTrace();
		} finally {
		    try {
		    	//Se cierra el archivo
		    	if (input!=null) input.close();
			} catch (IOException e) {
				System.out.println(MessageFormat.format(archivoErrorLectura, configFilePath));
			}
		    
		}
		
    }

}

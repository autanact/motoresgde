package co.net.une.www.motores;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: GeoreferenciaMasivaTask.java
##	Contenido: Clase que implementa la tarea de ejecuci�n del motor de georreferencia masiva
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Creaci�n de la clase
##**********************************************************************************************************************************
*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;

import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.util.GDEException;
import co.net.une.www.util.UtilGDE;
import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;

/**
 * Clase que implementa la tarea de ejecuci�n del motor de georreferencia masiva
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class GeoreferenciaMasivaTask extends TimerTask {
	
	//archivo de propiedades
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.MotorGeorrefenciaMasiva", Locale.getDefault());
	
	//archivo de configuraci�n
	private static final String configFilePath = ServiciosUtil.getMotorGeoConfigFile();
	private static final String nombreAmbiente = ServiciosUtil.getNombreAmbiente();
	
	//errores
	private static final String archivoNoExiste = rb.getString("error.archivo.archivoNoExiste");
	private static final String archivoErrorLectura = rb.getString("error.archivo.archivoErrorLectura");
	private static final String messageGDEError = rb.getString("error.messageGDEError");
	
	private int periodicidad = 0;
	private ServletContext servletContext;
	
	public GeoreferenciaMasivaTask(int periodicidad) {
		super();
		this.periodicidad = periodicidad;
		System.out.println("Se configura en motor GeoreferenciaMasivaTask en: "+nombreAmbiente);
	}
	
	public GeoreferenciaMasivaTask(ServletContext servletContext) {
		super();
		this.periodicidad = 0;
		this.setServletContext(servletContext);
		System.out.println("Se configura en motor GeoreferenciaMasivaTask en: "+nombreAmbiente);
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public int getPeriodicidad() {
		return periodicidad;
	}

	public void setPeriodicidad(int periodicidad) {
		this.periodicidad = periodicidad;
	}



	public void run()
    {
		System.out.println("**********************");
		System.out.println("Inicia motor GeoreferenciaMasivaTask: "+getPeriodicidad());
		
		int periAux = getPeriodicidad();
		
        //Coloco el formato de hora y minutos para la hora actual
        Calendar cal = Calendar.getInstance();
        Date defaultDate = new Date();
        cal.setTime(defaultDate);
		
        Properties propiedades = null;
        InputStream input = null;
		//BufferedReader br = null;
		try {
			
			//formateo la fecha por defecto
	        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
	        String horaAct = sdf.format(defaultDate);
	        defaultDate = UtilGDE.convertirObjetoFecha("HHmm", horaAct);
	        
			//Leo el archivo de propiedades
	        input = new FileInputStream(configFilePath);
	        
			//cargo la informaci�n desde el archivo de propiedades
	        propiedades = new Properties();
			propiedades.load(input);
	        
			//br = new BufferedReader(new FileReader(configFilePath));
			System.out.println("Se configura en: "+configFilePath);
		   // String line = br.readLine();
		    
		    //obtengo las variables del archivo de propiedades
		    //obtengo la hora de inicio de proceso
		    String horaInicio = propiedades.getProperty("hora_inicio");
		    UtilGDE.convertirObjetoFecha("HHmm", horaInicio);
		   // line = br.readLine();
		    //obtengo la hora final del proceso
		    String horaFinal =  propiedades.getProperty("hora_fin");
		    Date horaFin = UtilGDE.convertirObjetoFecha("HHmm", horaFinal);
		   // line = br.readLine();
		    //obtengo la periodicidad
		    String periodicidad  =  propiedades.getProperty("periodicidad");
		    int repeticion = Integer.parseInt((String) getServletContext().getAttribute("motorGeoRep"));
		    
		    if (repeticion == getPeriodicidad()) {

		    	// Valido que soy el unico que estoy funcionando
		    	
		        //verifico si el proceso est� dentro del rango
		        if (!UtilGDE.estaEnRango(horaAct, horaInicio, horaFinal)){
		        	
		        	//Estoy fuera del rango para obtener la pr�xima fecha para la ejecuci�n
		        	defaultDate = obtenerProximaFecha(defaultDate, horaFin, horaInicio);
		        } else {
		        	
		        	//Estoy en el rango, verifico la periodicidad
		        	MotorGeorrefenciaMasiva.ejecutarMotor();
		        	//Se vuelve a ejecutar
		        	Date ahora = new Date();
			        Calendar c1 = Calendar.getInstance();
			        c1.setTime(ahora);
			     // La periodicidad se calcula en horas
			        c1.add(Calendar.HOUR, Integer.parseInt(periodicidad));
		        	defaultDate = c1.getTime();
		        }
		        
			    //Configurar pr�xima ejecuci�n del servicio
			    configurarProximaEjecucion(defaultDate,periAux);
		    }
	        
		    
		} catch (FileNotFoundException e) {
			//No se encontr� el archivo de configuraci�n
			System.out.println(MessageFormat.format(archivoNoExiste,configFilePath));
			
		} catch (IOException e) {
			//No se pudo leer el archivo de configuraci�n
			System.out.println(MessageFormat.format(archivoErrorLectura, configFilePath));

		} catch (GDEException e) {
			System.out.println(messageGDEError+" "+e.getMensage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
		    try {
		    	//Se cierra el archivo
		    	if (input!=null) input.close();
			} catch (IOException e) {
				System.out.println(MessageFormat.format(archivoErrorLectura, configFilePath));
			}

		}
		
    }

	/**
	 * M�todo para agendar la pr�xima corrida de la ejecuci�n
	 * @param defaultDate
	 * 			Fecha del nuevo proceso
	 * @param periAux
	 * 			Repitici�n del proceso
	 */
	private void configurarProximaEjecucion(Date defaultDate, int periAux) {
		
		//obtengo la hora actual
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date now = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		
		Calendar c1 = Calendar.getInstance();
		c1.setTime(defaultDate);
		
		//Inicializo las variales
	    Timer timer = new Timer();
		int nextRep = periAux+1;
	    GeoreferenciaMasivaTask inicio = new GeoreferenciaMasivaTask(getServletContext());
		inicio.setPeriodicidad(nextRep);
		inicio.getServletContext().removeAttribute("motorGeoRep");
		inicio.getServletContext().setAttribute("motorGeoRep", nextRep+"");
	    
	    //Muestro la hora actual
	    System.out.println("**********************");
	    System.out.println("Hora Actual: "+ sdf.format(now.getTime()));	 
	    
	    //Muestro la ejecuci�n de la tarea     
	    System.out.println("Pr�xima hora de ejecuci�n motor GeoreferenciaMasivaTask: "+sdf.format(c1.getTime()));
	    
	    //Agendo el proceso
	    timer.schedule(inicio, c1.getTime());
		
	}
	
	
	/**
	 * M�todo que se utiliza para obtener la pr�xima fecha
	 * @param defaultDate
	 * 			Fecha actual
	 * @param horaFin
	 * 			Fecha fin del rango de fecha
	 * @param horaInicio
	 * 			Fecha inicio del rango de fecha
	 * @return
	 * 			Nueva fecha
	 * @throws GDEException
	 */
	private Date obtenerProximaFecha(Date defaultDate, Date horaFin, String horaInicio) throws GDEException{
		//Le coloco le fecha inicial
		//Veo si la ejecuci�n termin� el mismo d�as
		boolean mismoDia = (TimeUnit.DAYS.convert(defaultDate.getTime()-horaFin.getTime(), TimeUnit.MILLISECONDS)==0);
		
		//verifico si todav�a la hora final est� en el mismo d�a
		Calendar c1 = Calendar.getInstance();
		Date hoy = new Date();
		c1.setTime(hoy);
		String nextHour = c1.get(Calendar.DAY_OF_MONTH)+"/"+(c1.get(Calendar.MONTH)+1)+"/"+c1.get(Calendar.YEAR)+" "+horaInicio;
		c1.setTime(UtilGDE.convertirObjetoFecha("dd/MM/yyyy HHmm", nextHour));
		if (mismoDia){
			
			//es el mismo dia, la pr�xima ejecuci�n ser� al d�a siguiente a la hora de inicio
			c1.add(Calendar.DATE,1);
		}
		
		return c1.getTime();
	}
		
}

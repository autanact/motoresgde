package co.net.une.www.motores;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: AlmacenamientoHistoricoTask.java
##	Contenido: Clase que implementa la tarea de ejecuci�n del motor de almacenamiento hist�rico
##	Autor: Freddy Molina
## Fecha creaci�n: 31-01-2016
##	Fecha �ltima modificaci�n: 31-01-2016
##	Historial de cambios:  
##		31-01-2016 FMS Creaci�n de la clase
##**********************************************************************************************************************************
*/
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;

import co.net.une.ejb43.util.ServiciosUtil;
import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;

/**
 * Clase que implementa la tarea de ejecuci�n del motor de almacenamiento hist�rico.
 * @author Freddy Molina
 * @creado 31-01-2016
 * @ultimamodificacion 31-01-2016
 * @version 1.0
 * @historial
 * 			31-01-2016 FJM Primera versi�n
 */
public class AlmacenamientoHistoricoTask extends TimerTask {
	
	//archivo de propiedades
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.MotorAlmacenamientoHistorico", Locale.getDefault());
	
	//variables de configuraci�n
	//archivo de configuraci�n
	private static final String configFilePath = ServiciosUtil.getMotorHistoricoConfigFile();
	private static final String nombreAmbiente = ServiciosUtil.getNombreAmbiente();
	
	private static final String horaDefault = rb.getString("horaDefault");
	//errores
	private static final String archivoNoExiste = rb.getString("error.configFile.archivoNoExiste");
	private static final String archivoErrorLectura = rb.getString("error.configFile.archivoErrorLectura");
	
	private int repeticion = 0;
	private ServletContext servletContext;
	
	public AlmacenamientoHistoricoTask(ServletContext servletContext) {
		super();
		setRepeticion(0);
		this.setServletContext(servletContext);
	    System.out.println("Se configura en motor AlmacenamientoHistoricoTask en: "+nombreAmbiente);
	}
	
	public AlmacenamientoHistoricoTask(int reperticion) {
		super();
		setRepeticion(reperticion);
	    System.out.println("Se configura en motor AlmacenamientoHistoricoTask en: "+nombreAmbiente);
	}
	
	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public int getRepeticion() {
		return repeticion;
	}

	public void setRepeticion(int repeticion) {
		this.repeticion = repeticion;
	}

	public void run()
    {
		//Leo el archivo de propiedades
		//BufferedReader br = null;
		//hora de ejecuci�n
		String horaEjec = horaDefault;
		Date now = new Date();
		System.out.println("**********************");
		System.out.println("Inicia motor AlmacenamientoHistoricoTask");
		int periAux = getRepeticion();
		
        Properties propiedades = null;
        InputStream input = null;
        
		try {
			//Leo el archivo de propiedades
	        input = new FileInputStream(configFilePath);
			
			//cargo la informaci�n desde el archivo de propiedades
	        propiedades = new Properties();
			propiedades.load(input);
			
		    horaEjec=propiedades.getProperty("hora_inicio");
		    
		    int repeticion = Integer.parseInt((String) getServletContext().getAttribute("motorHisRep"));
		    
		    if (repeticion == getRepeticion()) {
		    	
		    	// Valido que soy el unico que estoy funcionando

			    MotorAlmacenamientoHistorico.ejecutarMotor();
			    
			    //Configurar pr�xima ejecuci�n del servicio
			    configurarProximaEjecucion(horaEjec, now,periAux);
		    } else{
			    //System.out.println("Hay un proceso m�s nuevo de Historico ME MUERO");
			}
		} catch (FileNotFoundException e) {
			//No se encontr� el archivo de configuraci�n
			System.out.println(MessageFormat.format(archivoNoExiste,configFilePath));
			
		} catch (IOException e) {
			//No se pudo leer el archivo de configuraci�n
			System.out.println(MessageFormat.format(archivoErrorLectura, configFilePath));

		} finally {
		    try {
		    	//Se cierra el archivo
		    	if (input!=null) input.close();
			} catch (IOException e) {
				System.out.println(MessageFormat.format(archivoErrorLectura, configFilePath));
			}
		}
    }
	
	/**
	 * M�todo que configura la pr�xima ejecuci�n al d�a siguiente
	 * @param horaEjec
	 * 			hora de la pr�xima ejecuci�n
	 * @param horaInicio
	 * 			hora a la cual se inici� el proceso
	 */
	private void configurarProximaEjecucion(String horaEjec, Date horaInicio, int periAux){
		
		//obtengo la hora actual
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date now = new Date();
		Calendar c = Calendar.getInstance();
		
		//Veo si la ejecuci�n termin� el mismo d�as
		boolean mismoDia = (TimeUnit.DAYS.convert(now.getTime()-horaInicio.getTime(), TimeUnit.MILLISECONDS)==0);
		
		//El proceso termin� el mismo d�a
		if (mismoDia){
			
			//la pr�xima ejecuci�n ser� al d�a siguiente, misma hora
			c.setTime(horaInicio);
			c.add(Calendar.DATE,1);
		}else{
			
			//la pr�xima ejecuci�n deber�a realizarce el mismo d�a
			
			String nextHour = c.get(Calendar.DAY_OF_MONTH)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR)+" "+horaEjec;
			try {
				//Actualizo la fecha de ejecuci�n
				c.setTime(sdf.parse(nextHour));
			} catch (ParseException e) {}
			
		}
		
		//Inicializo las variales
	    Timer timer = new Timer();
		int nextRep = periAux+1;
	    AlmacenamientoHistoricoTask inicio = new AlmacenamientoHistoricoTask(getServletContext());
		inicio.setRepeticion(nextRep);
		inicio.getServletContext().removeAttribute("motorHisRep");
		inicio.getServletContext().setAttribute("motorHisRep", nextRep+"");
	    
	    //Muestro la hora actual
	    System.out.println("**********************");
	    System.out.println("Hora Actual: "+ sdf.format(now.getTime()));	   
	    
	    //Muestro la ejecuci�n de la tarea     
	    System.out.println("Pr�xima hora de ejecuci�n motor AlmacenamientoHistoricoTask: "+sdf.format(c.getTime()));
	    
	    //Agendo el proceso
	    timer.schedule(inicio, c.getTime());
	}

}

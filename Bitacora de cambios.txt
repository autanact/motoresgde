#Bitacora de cambios Clases Motores GDE
#Freddy Molina
#Fecha creaci�n:13/12/2015
#Descripi�n general: Clases que manejan los motores y otros insumos del proyecto GDE (Gesti�n de direcciones excepcionadas)
#�ltima modificaci�n
	13/12/2015 FMS Creaci�n de Motores proyecto GDE
	11/02/2016 FMS Creaci�n de versi�n 1.0. Publicada en ambientes DESA/PRE/PROD  
	13/02/2016 FMS Actualizaci�n versi�n 2.0  incluye estado E (v1.0 respaldada 2016/02/13)
	13/02/2016 FMS Se respalda 2016/02/13 la versi�n de PROD actual
	23/02/2016 FMS Se respalda 2016/02/23 la versi�n de PRE actual
	23/02/2016 FMS Se publica en desarrollo versi�n con archivo de propiedades
	29/02/2016 FMS Se documentan todos los properties. Esta versi�n est� respalda en 2016/02/29
	01/03/2016 FMS Se comentan todos los c�digos y depuran. Versi�n actual DESA/PRE respaldada en 2016/03/01
#Versiones
	11/02/2016 v1.0	PC/DESA/PRE/PROD
	13/02/2016 v2.0 PC/